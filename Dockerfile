FROM node:10-slim as prod
ENV NODE_ENV=production

WORKDIR /app

COPY package*.json ./
RUN npm install --only production && npm cache clean --force
COPY . .

FROM prod as dev
ENV NODE_ENV=development
RUN npm install --only development
CMD ["../node_modules/nodemon/bin/nodemon.js", "./main.js", "--inspect=0.0.0.0:922"]

FROM dev as test
ENV NODE_ENV=test
ENV API_ENDPOINT=http://kong:8000
CMD ["npm", "test"]
