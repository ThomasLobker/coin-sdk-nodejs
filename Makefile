include Makefile.mk

NAME=coin_sdk_nodejs
IS_RELEASE := $(shell . $(RELEASE_SUPPORT) ; differsFromRelease || echo "release")

push:
ifdef IS_RELEASE
	@echo '//registry.npmjs.org/:_authToken='$(shell AWS_REGION=eu-central-1 AWS_PROFILE=git-api ssm-get-parameter --parameter-name /alm/npm/devops_coin/access_token) > ~/.npmrc
	npm install
	npm run-script build
	npm publish --access public
	rm ~/.npmrc
else
	@echo "Skipping a snapshot deploy. Only release builds are deployed. To release, run 'make tag-xx-release'"
endif
