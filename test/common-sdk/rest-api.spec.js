const RestApiClient = require('../../src/common-sdk/rest-api');
const coinBaseUrl = "http://kong:8000";
const consumerName = "loadtest-loada";
const privateKeyFile = "keys/private-key.pem";
const encryptedHmacSecretFile = "keys/sharedkey.encrypted";
const validPeriodInSeconds = 30;

describe('rest-api', () => {
  it('should connect with backend', (done) => {
    const api = new RestApiClient(consumerName, privateKeyFile, encryptedHmacSecretFile, validPeriodInSeconds);

    const req = api
          .sendWithToken('GET', coinBaseUrl+"/number-portability/v1/dossiers/events", undefined, {'accept':'text/event-stream'});

    req.on('data', function() {
      req.cancel();
      done();
    });
  });
});
