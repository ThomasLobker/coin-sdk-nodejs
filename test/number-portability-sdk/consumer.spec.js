const utils = require('../../src/common-sdk/utils');
const NumberPortabilityMessageConsumer = require('../../src/number-portability-sdk/service/consumer');
const chai = require('chai');
const { expect } = chai;

const coinBaseUrl = require('../../src/common-sdk/env').get("API_ENDPOINT");
const consumerName = "loadtest-loada";
const privateKeyFile = "keys/private-key.pem";
const encryptedHmacSecretFile = "keys/sharedkey.encrypted";
const validPeriodInSeconds = 30;

describe('consumer', () => {
  it('should receive all messages', (done) => {
    this.consumer = new NumberPortabilityMessageConsumer(consumerName, privateKeyFile, encryptedHmacSecretFile, coinBaseUrl, 5, 3, validPeriodInSeconds);
    const msgs = {};
    const deferred = new utils.Deferred();
    const recordMessageArrived = (message) => {
      msgs[message.type] = message;
      // when we have received as many unique messages as there are handlers, we are done
      if (Object.keys(msgs).length == Object.keys(handlers).length)
        deferred.resolve();
    };
    const handlers = {
      onActivationsn: recordMessageArrived,
      onCancel: recordMessageArrived,
      onDeactivation: recordMessageArrived,
      onDeactivationsn: recordMessageArrived,
      onEnumActivationNumber: recordMessageArrived,
      onEnumActivationOperator: recordMessageArrived,
      onEnumActivationRange: recordMessageArrived,
      onEnumDeactivationNumber: recordMessageArrived,
      onEnumDeactivationOperator: recordMessageArrived,
      onEnumDeactivationRange: recordMessageArrived,
      onEnumProfileActivation: recordMessageArrived,
      onEnumProfileDeactivation: recordMessageArrived,
      // onErrorFound: recordMessageArrived,
      onPortingPerformed: recordMessageArrived,
      onPortingRequest: recordMessageArrived,
      onPortingRequestAnswer: recordMessageArrived,
      // onPortingRequestAnswerDelayed: recordMessageArrived,
      onRangeActivation: recordMessageArrived,
      onRangeDeactivation: recordMessageArrived,
      onTariffChangeSn: recordMessageArrived,
    };
    const disposable = this.consumer.startConsuming(handlers);
    deferred.promise.then(() => {
      // clear the dead-man-switch function
      clearTimeout(timer);
      disposable.close();
      done();
    });
    // here setup a dead-man-switch function
    // we expect this never to be called unless at least one message doesn't arrive
    const timer = setTimeout(()=>{
      disposable.close();
      expect(Object.keys(msgs)).to.equal(Object.keys(handlers).map(h => h.toLowerCase()+'-v1'));
    }, 20000);
  }).timeout(60000);
});
