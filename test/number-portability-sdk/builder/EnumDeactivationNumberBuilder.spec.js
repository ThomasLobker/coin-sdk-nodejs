const expect = require('./common/expect');

const EnumDeactivationNumberBuilder = require('../../../src/number-portability-sdk/messages/v1/builder/EnumDeactivationNumberBuilder');
const MessageTypeEnum = require('../../../src/number-portability-sdk/messages/v1/messagetype.js');

describe('EnumDeactivationNumberBuilder', () => {
  xit('message should be of type enumdeactivationnumber', () => {
    const result = '{"message":{"header":{"receiver":{"networkoperator":"DEMO2"},"sender":{"networkoperator":"DEMO1"}},"body":{"enumdeactivationnumber":{"dossierid":"DEMO1-123456","currentnetworkoperator":"DEMO1","typeofnumber":"X","repeats":[{"seq":{"numberseries":{"start":"0123456789","end":"0987654321"},"repeats":[{"seq":{"profileid":"PROF1"}},{"seq":{"profileid":"PROF2"}}]}}]}}}}';
    const builder = new EnumDeactivationNumberBuilder();
    const message = builder
      .setHeader("DEMO1", "DEMO2")
      .setDossierId("DEMO1-123456")
      .setCurrentNetworkOperator("DEMO1")
      .setTypeOfNumber('X')
      .addEnumNumberSequence()
      .setNumberSeries("0123456789", "0987654321")
      .setProfileIds(['PROF1', 'PROF2'])
      .finish()
      .build();

    expect(message.getMessageType()).to.equal(MessageTypeEnum.enumdeactivationnumber);
    expect(JSON.stringify(message.getMessage())).to.equal(result);
  });
});
