const expect = require('./common/expect');

const PortingRequestAnswerDelayedBuilder = require('../../../src/number-portability-sdk/messages/v1/builder/PortingRequestAnswerDelayedBuilder');
const MessageTypeEnum = require('../../../src/number-portability-sdk/messages/v1/messagetype');

describe('PortingRequestAnswerDelayedBuilder', () => {
  xit('message should be of type pradelayed', () => {
    const result = '{"message":{"header":{"receiver":{"networkoperator":"DEMO2"},"sender":{"networkoperator":"DEMO1"}},"body":{"pradelayed":{"dossierid":"DEMO1-123456","donornetworkoperator":"DEMO2","donorserviceprovider":"DEMO2","answerduedatetime":"X","reasoncode":"99"}}}}';
    const builder = new PortingRequestAnswerDelayedBuilder();
    const message = builder
      .setHeader('DEMO1', 'DEMO2')
      .setDossierId("DEMO1-123456")
      .setDonorNetworkOperator("DEMO2")
      .setDonorServiceProvider("DEMO2")
      .setAnswerDueDateTime("X")
      .setReasonCode("99")
      .build();

    // console.log(JSON.stringify(message.getMessage()));
    expect(message.getMessageType()).to.equal(MessageTypeEnum.pradelayed);
    expect(JSON.stringify(message.getMessage())).to.equal(result);
  });
});
