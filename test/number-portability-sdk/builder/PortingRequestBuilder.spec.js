const expect = require('./common/expect');

const PortingRequestBuilder = require('../../../src/number-portability-sdk/messages/v1/builder/PortingRequestBuilder');
const MessageTypeEnum = require('../../../src/number-portability-sdk/messages/v1/messagetype');

const results = require('./results/PortingRequestMessage');
const buildMessage = require('./messages/PortingRequestMessage');

function fixResult(result, actual) {
  result.message.header.timestamp = actual.message.header.timestamp;
  return result;
}

describe('PortingRequestBuilder', () => {
  it('message should be of type portingrequest', () => {
    const offset = 0;
    const builder = new PortingRequestBuilder();
    const message = buildMessage(offset, builder);

    expect(message.getMessageType()).to.equal(MessageTypeEnum.portingrequest);
  });

  it('message should have correct structure', () => {
    const offset = 0;
    const builder = new PortingRequestBuilder();
    const message = buildMessage(offset, builder);
    const expected = results[offset];
    const actual = message.getMessage();

    expect(actual).to.deep.equalInAnyOrder(fixResult(expected, actual));
  });
});
