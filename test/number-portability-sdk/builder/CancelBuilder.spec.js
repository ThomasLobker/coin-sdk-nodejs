const expect = require('./common/expect');

const CancelBuilder = require('../../../src/number-portability-sdk/messages/v1/builder/CancelBuilder');
const MessageTypeEnum = require('../../../src/number-portability-sdk/messages/v1/messagetype');

const results = require('./results/CancelMessage');
const buildMessage = require('./messages/CancelMessage');

function fixResult(result, actual) {
  result.message.header.timestamp = actual.message.header.timestamp;
  return result;
}

describe('CancelBuilder', () => {
  it('message should be of type cancel', () => {
    const builder = new CancelBuilder();
    const message = buildMessage(0, builder);

    expect(message.getMessageType()).to.equal(MessageTypeEnum.cancel);
  });

  it('message should have the correct structure', () => {
    const offset = 0;
    const builder = new CancelBuilder();
    const message = buildMessage(offset, builder);
    const expected = results[offset];
    const actual = message.getMessage();

    expect(actual).to.deep.equalInAnyOrder(fixResult(expected, actual));
  });

  it('message should have header and note', () => {
    const offset = 1;
    const builder = new CancelBuilder();
    const message = buildMessage(offset, builder);
    const expected = results[offset];
    const actual = message.getMessage();

    expect(actual).to.deep.equalInAnyOrder(fixResult(expected, actual));
  });

  it('message should have full header without a note', () => {
    const offset = 1;
    const builder = new CancelBuilder();
    const message = buildMessage(offset, builder);
    const expected = results[offset];
    const actual = message.getMessage();

    expect(actual).to.deep.equalInAnyOrder(fixResult(expected, actual));
  });
});
