const expect = require('./common/expect');

const PortingPerformedBuilder = require('../../../src/number-portability-sdk/messages/v1/builder/PortingPerformedBuilder');
const MessageTypeEnum = require('../../../src/number-portability-sdk/messages/v1/messagetype');

describe('PortingPerformedBuilder', () => {
  xit('message should be of type portingperformed', () => {
    const result = '{"message":{"header":{"receiver":{"networkoperator":"DEMO2"},"sender":{"networkoperator":"DEMO1"}},"body":{"portingperformed":{"dossierid":"DEMO1-123456","recipientnetworkoperator":"DEMO2","donornetworkoperator":"DEMO2","repeats":[{"seq":{"numberseries":{"start":"06123456789","end":"06123456789"},"backporting":"N","pop":"pop","repeats":[{"seq":{"profileid":"PROF1"}},{"seq":{"profileid":"PROF2"}}]}}],"actualdatetime":"X","batchporting":"N"}}}}';
    const builder = new PortingPerformedBuilder();
    const message = builder
      .setHeader('DEMO1', 'DEMO2')
      .setDossierId('DEMO1-123456')
      .setRecipientNetworkOperator('DEMO2')
      .setDonorNetworkOperator('DEMO2')
      .setActualDateTime('X')
      .setBatchPorting('N')
      .addPortingPerformedSequence()
      .setNumberSeries("06123456789", "06123456789")
      .setBackPorting("N")
      .setPop("pop")
      .setProfileIds(['PROF1', 'PROF2'])
      .finish()
      .build();

    expect(message.getMessageType()).to.equal(MessageTypeEnum.portingperformed);
    expect(JSON.stringify(message.getMessage())).to.equal(result);
  });
});
