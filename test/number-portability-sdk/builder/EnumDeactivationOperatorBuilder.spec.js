const expect = require('./common/expect');

const EnumDeactivationOperatorBuilder = require('../../../src/number-portability-sdk/messages/v1/builder/EnumDeactivationOperatorBuilder');
const MessageTypeEnum = require('../../../src/number-portability-sdk/messages/v1/messagetype.js');

describe('EnumDeactivationOperatorBuilder', () => {
  xit('message should be of type enumdeactivationoperator', () => {
    const result = '{"message":{"header":{"receiver":{"networkoperator":"DEMO2"},"sender":{"networkoperator":"DEMO1"}},"body":{"enumdeactivationoperator":{"dossierid":"DEMO1-123456","currentnetworkoperator":"DEMO1","typeofnumber":"X","repeats":[{"seq":{"profileid":"PROF1","defaultservice":"X"}}]}}}}';
    const builder = new EnumDeactivationOperatorBuilder();
    const message = builder
      .setHeader("DEMO1", "DEMO2")
      .setDossierId("DEMO1-123456")
      .setCurrentNetworkOperator("DEMO1")
      .setTypeOfNumber('X')
      .addEnumOperatorSequence()
      .setProfileId("PROF1")
      .setDefaultService('X')
      .finish()
      .build();

    expect(message.getMessageType()).to.equal(MessageTypeEnum.enumdeactivationoperator);
    expect(JSON.stringify(message.getMessage())).to.equal(result);
  });
});
