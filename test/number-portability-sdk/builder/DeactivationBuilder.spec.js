const expect = require('./common/expect');

const DeactivationBuilder = require('../../../src/number-portability-sdk/messages/v1/builder/DeactivationBuilder');
const MessageTypeEnum = require('../../../src/number-portability-sdk/messages/v1/messagetype.js');

const results = require('./results/DeactivationMessage');
const buildMessage = require('./messages/DeactivationMessage');

function fixResult(result, actual) {
  result.message.header.timestamp = actual.message.header.timestamp;
  return result;
}

describe('DeactivationBuilder', () => {
  it('message should be of type activationsn', () => {
    const builder = new DeactivationBuilder();
    const message = buildMessage(0, builder);

    expect(message.getMessageType()).to.equal(MessageTypeEnum.deactivation);
  });

  it('message should have the correct structure', () => {
    const offset = 0;
    const builder = new DeactivationBuilder();
    const message = buildMessage(offset, builder);
    const expected = results[offset];
    const actual = message.getMessage();

    expect(actual).to.deep.equalInAnyOrder(fixResult(expected, actual));
  });
});
