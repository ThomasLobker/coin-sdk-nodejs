const expect = require('./common/expect');

const TariffChangeServiceNumberBuilder = require('../../../src/number-portability-sdk/messages/v1/builder/TariffChangeServiceNumberBuilder');
const MessageTypeEnum = require('../../../src/number-portability-sdk/messages/v1/messagetype');

const results = require('./results/TariffChangeServiceNumberMessage');
const buildMessage = require('./messages/TariffChangeServiceNumberMessage');

function fixResult(result, actual) {
  result.message.header.timestamp = actual.message.header.timestamp;
  return result;
}

describe('TariffChangeServiceNumberBuilder', () => {
  it('message should be of type tariffchangesn', () => {
    const offset = 0;
    const builder = new TariffChangeServiceNumberBuilder();
    const message = buildMessage(offset, builder);

    expect(message.getMessageType()).to.equal(MessageTypeEnum.tariffchangesn);
  });

  it('message should have correct structure', () => {
    const offset = 0;
    const builder = new TariffChangeServiceNumberBuilder();
    const message = buildMessage(offset, builder);
    const expected = results[offset];
    const actual = message.getMessage();

    expect(actual).to.deep.equalInAnyOrder(fixResult(expected, actual));
  });
});
