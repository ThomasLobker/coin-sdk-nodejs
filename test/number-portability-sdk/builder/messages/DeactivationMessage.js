module.exports = function buildMessage(offset, builder) {
  let message;
  switch (offset) {
    case 0:
      message = builder
        .setHeader("DEMO1", "DEMO2")
        .setDossierId("DEMO1-123456")
        .setCurrentNetworkOperator("DEMO1")
        .setOriginalNetworkOperator("DEMO1")
        .addDeactivationSequence()
        .setNumberSeries("0123456789", "0987654321")
        .finish()
        .addDeactivationSequence()
        .setNumberSeries("0123456789", "0987654321")
        .finish()
        .build();
      break;
    default:
      console.error(`buildMessage: invalid offset=${offset}`);
      break;
  }
  return message;
};

