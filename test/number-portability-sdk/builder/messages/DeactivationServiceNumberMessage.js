module.exports = function buildMessage(offset, builder) {
  let message;
  switch (offset) {
    case 0:
      message = builder
        .setHeader("DEMO1", "DEMO2")
        .setDossierId("DEMO1-123456")
        .setPlatformProvider("DEMO1")
        .setPlannedDateTime("20190101120000")
        .addDeactivationServiceNumberSequence()
        .setNumberSeries("0123456789", "0987654321")
        .setPop("pop")
        .finish()
        .build();
      break;
    default:
      console.error(`buildMessage: invalid offset=${offset}`);
      break;
  }
  return message;
};
