module.exports = function buildMessage(offset, builder) {
  let message;
  switch (offset) {
    case 0:
      message = builder
        .setHeader('DEMO1', 'DEMO2')
        .setDossierId('DEMO1-123456')
        .setRecipientNetworkOperator('DEMO2')
        .setRecipientServiceProvider('DEMO2')
        .setCustomerInfo('Lastname', 'Company', '10', 'a', '1234AA', '12')
        .addPortingRequestSequence()
        .setNumberSeries('06123456799', '06123456799')
        .setProfileIds(['PROF1', 'PROF2'])
        .finish()
        .build();
      break;
    default:
      console.error(`buildMessage: invalid offset=${offset}`);
      break;
  }
  return message;
};

