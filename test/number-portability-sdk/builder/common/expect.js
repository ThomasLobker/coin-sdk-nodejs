/* eslint-disable-next-line node/no-unpublished-require */
const chai = require('chai');
/* eslint-disable-next-line node/no-unpublished-require */
const deepEqualInAnyOrder = require('deep-equal-in-any-order');
chai.use(deepEqualInAnyOrder);
const { expect } = chai;

module.exports = expect;
