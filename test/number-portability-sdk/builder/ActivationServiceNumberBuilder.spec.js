const expect = require('./common/expect');

const ActivationServiceNumberBuilder = require('../../../src/number-portability-sdk/messages/v1/builder/ActivationServiceNumberBuilder');
const MessageTypeEnum = require('../../../src/number-portability-sdk/messages/v1/messagetype.js');

const results = require('./results/ActivationServiceNumberMessage');
const buildMessage = require('./messages/ActivationServiceNumberMessage');

function fixResult(result, actual) {
  result.message.body.activationsn.planneddatetime = actual.message.body.activationsn.planneddatetime;
  result.message.header.timestamp = actual.message.header.timestamp;
  return result;
}

describe('ActiviationServiceNumberBuilder', () => {
  it('message should be of type activationsn', () => {
    const offset = 0;
    const builder = new ActivationServiceNumberBuilder();
    const message = buildMessage(offset, builder);

    expect(message.getMessageType()).to.equal(MessageTypeEnum.activationsn);
  });

  it('message should have correct structure', () => {
    const offset = 0;
    const builder = new ActivationServiceNumberBuilder();
    const message = buildMessage(offset, builder);
    const expected = results[offset];
    const actual = message.getMessage();

    expect(actual).to.deep.equalInAnyOrder(fixResult(expected, actual));
  });
});
