const expect = require('./common/expect');

const Builder = require('../../../src/number-portability-sdk/messages/v1/builder/DeactivationServiceNumberBuilder');
const MessageTypeEnum = require('../../../src/number-portability-sdk/messages/v1/messagetype.js');

const results = require('./results/DeactivationServiceNumberMessage');
const buildMessage = require('./messages/DeactivationServiceNumberMessage');

function fixResult(result, actual) {
  result.message.header.timestamp = actual.message.header.timestamp;
  return result;
}

describe('DeactivationServiceNumberBuilder', () => {
  it('message should be of type deactivationsn', () => {
    const builder = new Builder();
    const message = buildMessage(0, builder);

    expect(message.getMessageType()).to.equal(MessageTypeEnum.deactivationsn);
  });

  it('message should have the correct structure', () => {
    const offset = 0;
    const message = buildMessage(offset, new Builder());
    const expected = results[offset];
    const actual = message.getMessage();

    expect(actual).to.deep.equalInAnyOrder(fixResult(expected, actual));
  });
});
