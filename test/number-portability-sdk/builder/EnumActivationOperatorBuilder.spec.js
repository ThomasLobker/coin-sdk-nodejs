const expect = require('./common/expect');

const EnumActivationOperatorBuilder = require('../../../src/number-portability-sdk/messages/v1/builder/EnumActivationOperatorBuilder');
const MessageTypeEnum = require('../../../src/number-portability-sdk/messages/v1/messagetype.js');

describe('EnumActivationOperatorBuilder', () => {
  xit('message should be of type enumactivationoperator', () => {
    const result = '{"message":{"header":{"receiver":{"networkoperator":"DEMO2"},"sender":{"networkoperator":"DEMO1"}},"body":{"enumactivationoperator":{"dossierid":"DEMO1-123456","currentnetworkoperator":"DEMO1","typeofnumber":"X","repeats":[{"seq":{"profileid":"PROF1","defaultservice":"X"}}]}}}}';
    const builder = new EnumActivationOperatorBuilder();
    const message = builder
      .setHeader("DEMO1", "DEMO2")
      .setDossierId("DEMO1-123456")
      .setCurrentNetworkOperator("DEMO1")
      .setTypeOfNumber('X')
      .addEnumOperatorSequence()
      .setProfileId("PROF1")
      .setDefaultService('X')
      .finish()
      .build();

    expect(message.getMessageType()).to.equal(MessageTypeEnum.enumactivationoperator);
    expect(JSON.stringify(message.getMessage())).to.equal(result);
  });
});
