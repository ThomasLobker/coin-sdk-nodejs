const today = new Date();
const timestamp = today.getFullYear() +
  ("0" + (today.getMonth() + 1)).slice(-2) +
  ("0" + today.getDate()).slice(-2) +
  ("0" + today.getHours() + 1 ).slice(-2) +
  ("0" + today.getMinutes()).slice(-2) +
  ("0" + today.getSeconds()).slice(-2);

module.exports = [{
  /* 0 */
  "message": {
    "header": {
      "receiver": {
        "networkoperator": "DEMO2"
      },
      "sender": {
        "networkoperator": "DEMO1"
      },
      "timestamp": timestamp
    },
    "body": {
      "tariffchangesn": {
        "dossierid": "DEMO1-123456",
        "platformprovider": "DEMO2",
        "planneddatetime": "X",
        "repeats": [
          {
            "seq": {
              "numberseries": {
                "start": "06123456799",
                "end": "06123456799"
              },
              "tariffinfonew": {
                "peak": "1.0",
                "offpeak": "2.0",
                "currency": "3.0",
                "type": "4.0",
                "vat": "5.0"
              }
            }
          }
        ]
      }
    }
  }
}];
