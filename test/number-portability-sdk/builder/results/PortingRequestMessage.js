const today = new Date();
const timestamp = today.getFullYear() +
  ("0" + (today.getMonth() + 1)).slice(-2) +
  ("0" + today.getDate()).slice(-2) +
  ("0" + today.getHours() + 1 ).slice(-2) +
  ("0" + today.getMinutes()).slice(-2) +
  ("0" + today.getSeconds()).slice(-2);

module.exports = [{
  /* 0 */
  "message": {
    "header": {
      "receiver": {
        "networkoperator": "DEMO2"
      },
      "sender": {
        "networkoperator": "DEMO1"
      },
      "timestamp": timestamp
    },
    "body": {
      "portingrequest": {
        "dossierid": "DEMO1-123456",
        "recipientnetworkoperator": "DEMO2",
        "repeats": [
          {
            "seq": {
              "numberseries": {
                "start": "06123456799",
                "end": "06123456799"
              },
              "repeats": [
                {
                  "seq": {
                    "profileid": "PROF1"
                  }
                },
                {
                  "seq": {
                    "profileid": "PROF2"
                  }
                }
              ]
            }
          }
        ],
        "recipientserviceprovider": "DEMO2",
        "customerinfo": {
          "lastname": "Lastname",
          "companyname": "Company",
          "housenr": "10",
          "housenrext": "a",
          "postcode": "1234AA",
          "customerid": "12"
        }
      }
    }
  }
}];
