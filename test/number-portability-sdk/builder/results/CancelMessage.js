const today = new Date();
const timestamp = today.getFullYear() +
  ("0" + (today.getMonth() + 1)).slice(-2) +
  ("0" + today.getDate()).slice(-2) +
  ("0" + today.getHours() + 1 ).slice(-2) +
  ("0" + today.getMinutes()).slice(-2) +
  ("0" + today.getSeconds()).slice(-2);

module.exports = [
  /* 0 */
  {
    "message": {
      "header": {
        "receiver": { "networkoperator": "DEMO2" },
        "sender": { "networkoperator": "DEMO1" },
        "timestamp": timestamp
      },
      "body": {
        "cancel": {
          "dossierid": "DEMO1-123456",
          "note": "Example note"
        }
      }
    }
  },
  /* 1 */
  {
    "message": {
      "header": {
        "receiver": { "networkoperator": "DEMO2", "serviceprovider": "DEMO2" },
        "sender": { "networkoperator": "DEMO1", "serviceprovider": "DEMO1" },
        "timestamp": timestamp
      },
      "body": {
        "cancel": {
          "dossierid": "DEMO1-123456",
          "note": "Example note"
        }
      }
    }
  },
  /* 2 */
  {
    "message": {
      "header": {
        "receiver": { "networkoperator": "DEMO2", "serviceprovider": "DEMO2" },
        "sender": { "networkoperator": "DEMO1", "serviceprovider": "DEMO1" },
        "timestamp": timestamp
      },
      "body": {
        "cancel": {
          "dossierid": "DEMO1-123456"
        }
      }
    }
  }
];
