const today = new Date();
const timestamp = today.getFullYear() +
  ("0" + (today.getMonth() + 1)).slice(-2) +
  ("0" + today.getDate()).slice(-2) +
  ("0" + today.getHours() + 1 ).slice(-2) +
  ("0" + today.getMinutes()).slice(-2) +
  ("0" + today.getSeconds()).slice(-2);

module.exports = [
  /* 0 */
  {
    "message": {
      "header": {
        "receiver": {
          "networkoperator": "DEMO2"
        },
        "sender": {
          "networkoperator": "DEMO1"
        },
        "timestamp": timestamp
      },
      "body": {
        "deactivation": {
          "dossierid": "DEMO1-123456",
          "currentnetworkoperator": "DEMO1",
          "repeats": [
            {
              "seq": {
                "numberseries": {
                  "start": "0123456789",
                  "end": "0987654321"
                }
              }
            },
            {
              "seq": {
                "numberseries": {
                  "start": "0123456789",
                  "end": "0987654321"
                }
              }
            }
          ],
          "originalnetworkoperator": "DEMO1"
        }
      }
    }
  }
];
