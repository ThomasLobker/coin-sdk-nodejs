/* eslint-disable-next-line node/no-unpublished-require */
const chai = require('chai');
/* eslint-disable-next-line node/no-unpublished-require */
const deepEqualInAnyOrder = require('deep-equal-in-any-order');
chai.use(deepEqualInAnyOrder);
const { expect } = chai;
const chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);

const NumberPortabilityService = require('../../src/number-portability-sdk/service/service');
const PortingRequestBuilder = require('../../src/number-portability-sdk/messages/v1/builder/PortingRequestBuilder');
const PortingRequestAnswerBuilder = require('../../src/number-portability-sdk/messages/v1/builder/PortingRequestAnswerBuilder');
const PortingPerformedBuilder = require('../../src/number-portability-sdk/messages/v1/builder/PortingPerformedBuilder');

const dossierId = 'DEMO1-123456';

const consumerName = "loadtest-loada";
const privateKeyFile = "keys/private-key.pem";
const encryptedHmacSecretFile = "keys/sharedkey.encrypted";
const validPeriodInSeconds = 30;
const coinBaseUrl = require('../../src/common-sdk/env').get("API_ENDPOINT");

const senderNetworkOperator = 'DEMO1';
const senderServiceProvider = senderNetworkOperator;
const receiverNetworkOperator = 'DEMO2';
const receiverServiceProvider = receiverNetworkOperator;

const startNumberRange = "0612345678";
const endNumberRange = "0612345678";
const regExpTransactionId = /^[0-9a-z]{8}-[0-9a-z]{4}-[0-9a-z]{4}-[0-9a-z]{4}-[0-9a-z]{12}$/;

describe('service', () => {

  it('should sent a PortingRequest message and receive a transactionId', (done) => {
    this.service = new NumberPortabilityService(consumerName, privateKeyFile, encryptedHmacSecretFile, validPeriodInSeconds, coinBaseUrl);
    const builder = new PortingRequestBuilder();
    const message = builder
      .setFullHeader(senderNetworkOperator, senderServiceProvider, receiverNetworkOperator, receiverServiceProvider)
      .setDossierId(dossierId)
      .setRecipientNetworkOperator(senderNetworkOperator)
      .addPortingRequestSequence()
        .setNumberSeries(startNumberRange, endNumberRange)
        .finish()
      .build();

    const result = this.service.sendMessage(message);
    result.then(function(obj) {
      expect(obj.transactionId).to.match(regExpTransactionId);
      done();
    });
  });

  it('should sent a PortingRequestAnswer message and receive a transactionId', (done) => {
    this.service = new NumberPortabilityService(consumerName, privateKeyFile, encryptedHmacSecretFile, validPeriodInSeconds, coinBaseUrl);
    const builder = new PortingRequestAnswerBuilder();
    const message = builder
      .setFullHeader(senderNetworkOperator, senderServiceProvider, receiverNetworkOperator, receiverServiceProvider)
      .setDossierId(dossierId)
      .setBlocking("N")
      .addPortingRequestAnswerSequence()
        .setNumberSeries(startNumberRange, endNumberRange)
        .finish()
      .build();

    const result = this.service.sendMessage(message);
    result.then(function(obj) {
      expect(obj.transactionId).to.match(regExpTransactionId);
      done();
    });
  });

  it('should sent a PortingPerformed message and receive a transactionId', (done) => {
    this.service = new NumberPortabilityService(consumerName, privateKeyFile, encryptedHmacSecretFile, validPeriodInSeconds, coinBaseUrl);
    const builder = new PortingPerformedBuilder();
    const message = builder
      .setFullHeader(senderNetworkOperator, senderServiceProvider, receiverNetworkOperator, receiverServiceProvider)
      .setDossierId(dossierId)
      .setRecipientNetworkOperator(senderNetworkOperator)
      .setDonorNetworkOperator(receiverNetworkOperator)
      .addPortingPerformedSequence()
        .setNumberSeries(startNumberRange, endNumberRange)
        .finish()
      .build();

    const result = this.service.sendMessage(message);
    result.then(function(obj) {
      expect(obj.transactionId).to.match(regExpTransactionId);
      done();
    });
  });
  it('should sent a confirmation message', (done) => {
    this.service = new NumberPortabilityService(consumerName, privateKeyFile, encryptedHmacSecretFile, validPeriodInSeconds, coinBaseUrl);
    this.service.sendConfirmation('1234').then(()=>done());
  });
});
