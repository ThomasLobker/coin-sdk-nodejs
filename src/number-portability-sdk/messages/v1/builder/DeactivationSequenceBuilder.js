const DeactivationSeq = require('../model/DeactivationSeq');
const DeactivationRepeats = require('../model/DeactivationRepeats');
const NumberSeries = require('../model/NumberSeries');

class DeactivationSequenceBuilder {

  constructor(parent) {
    this.parent = parent;
    this.deactivationSequence = new DeactivationSeq();
  }

  setNumberSeries(start, end) {
    const numberSeries = new NumberSeries();
    numberSeries.start = start;
    numberSeries.end = end;
    this.deactivationSequence.numberseries = numberSeries;

    return this;
  }

  finish() {
    this.parent.deactivation.repeats.push(new DeactivationRepeats(this.deactivationSequence));
    return this.parent;
  }
}

module.exports = DeactivationSequenceBuilder;


