const Message = require('../message');
const MessageTypeEnum = require('../messagetype');
const MessageBuilder = require('./MessageBuilder');
const DeactivationSequenceBuilder = require('./DeactivationSequenceBuilder');
const DeactivationMessage = require('../model/DeactivationMessage');
const DeactivationBody = require('../model/DeactivationBody');
const Deactivation = require('../model/Deactivation');

class DeactivationBuilder extends MessageBuilder {

  constructor() {
    super();
    this.deactivation = new Deactivation();
    this.deactivation.repeats = [];
  }

  setDossierId(dossierId) {
    this.deactivation.dossierid = dossierId;
    return this;
  }

  setCurrentNetworkOperator(currentNetworkOperator) {
    this.deactivation.currentnetworkoperator = currentNetworkOperator;
    return this;
  }

  setOriginalNetworkOperator(originalNetworkOperator) {
    this.deactivation.originalnetworkoperator = originalNetworkOperator;
    return this;
  }

  addDeactivationSequence() {
    return new DeactivationSequenceBuilder(this);
  }

  build() {
    super.build();
    this.body = new DeactivationBody(this.deactivation);
    this.message = new DeactivationMessage(this.header, this.body);

    return new Message({"message":this.message}, MessageTypeEnum.deactivation);
  }
}

module.exports = DeactivationBuilder;


