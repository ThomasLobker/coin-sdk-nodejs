const Message = require('../message');
const MessageTypeEnum = require('../messagetype');
const MessageBuilder = require('./MessageBuilder');
const ActivationServiceNumberSequenceBuilder = require('./ActivationServiceNumberSequenceBuilder');
const ActivationServiceNumberMessage = require('../model/ActivationServiceNumberMessage');
const ActivationServiceNumberBody = require('../model/ActivationServiceNumberBody');
const ActivationServiceNumber = require('../model/ActivationServiceNumber');

class ActivationServiceNumberBuilder extends MessageBuilder {

  constructor() {
    super();
    this.activationservicenumber = new ActivationServiceNumber();
    this.activationservicenumber.repeats = [];
  }

  setDossierId(dossierId) {
    this.activationservicenumber.dossierid = dossierId;
    return this;
  }

  setNote(note) {
    this.activationservicenumber.note = note;
    return this;
  }

  setPlannedDateTime(plannedDateTime) {
    this.activationservicenumber.planneddatetime = plannedDateTime;
    return this;
  }

  setPlatformProvider(platformProvider) {
    this.activationservicenumber.platformprovider = platformProvider;
    return this;
  }

  addActivationServiceNumberSequence() {
    return new ActivationServiceNumberSequenceBuilder(this);
  }

  build() {
    super.build();
    this.body = new ActivationServiceNumberBody(this.activationservicenumber);
    this.message = new ActivationServiceNumberMessage(this.header, this.body);

    return new Message({"message":this.message}, MessageTypeEnum.activationsn);
  }
}

module.exports = ActivationServiceNumberBuilder;


