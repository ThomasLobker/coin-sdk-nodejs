const EnumRepeatsBuilder = require('./EnumRepeatsBuilder');
const NumberSeries = require('../model/NumberSeries');
const EnumNumberSeq = require('../model/EnumNumberSeq');
const EnumNumberRepeats = require('../model/EnumNumberRepeats');

class EnumNumberSequenceBuilder {

  constructor(parent) {
    this.parent = parent;
    this.enumNumberSequence = new EnumNumberSeq();
  }

  setNumberSeries(start, end) {
    const numberSeries = new NumberSeries();
    numberSeries.start = start;
    numberSeries.end = end;
    this.enumNumberSequence.numberseries = numberSeries;

    return this;
  }

  setProfileIds(profileIds) {
    const enumRepeats = new EnumRepeatsBuilder();
    enumRepeats.setProfileIds(profileIds);
    this.enumNumberSequence.repeats = enumRepeats.build();
    return this;
  }

  finish() {
    this.parent.enumcontent.repeats.push(new EnumNumberRepeats(this.enumNumberSequence));
    return this.parent;
  }

}

module.exports = EnumNumberSequenceBuilder;
