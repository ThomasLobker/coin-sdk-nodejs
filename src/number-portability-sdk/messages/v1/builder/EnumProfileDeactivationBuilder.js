const Message = require('../message');
const MessageTypeEnum = require('../messagetype');
const MessageBuilder = require('./MessageBuilder');
const EnumProfileDeactivation = require('../model/EnumProfileDeactivation');
const EnumProfileDeactivationBody = require('../model/EnumProfileDeactivationBody');
const EnumProfileDeactivationMessage = require('../model/EnumProfileDeactivationMessage');

class EnumProfileDeactivationBuilder extends MessageBuilder {

  constructor() {
    super();
    this.enumprofiledeactivation = new EnumProfileDeactivation();
  }

  setDossierId(dossierId) {
    this.enumprofiledeactivation.dossierid = dossierId;
    return this;
  }

  setCurrentNetworkOperator(currentNetworkOperator) {
    this.enumprofiledeactivation.currentnetworkoperator = currentNetworkOperator;
    return this;
  }

  setTypeOfNumber(typeOfNumber) {
    this.enumprofiledeactivation.typeofnumber = typeOfNumber;
    return this;
  }

  setProfileId(profileId) {
    this.enumprofiledeactivation.profileid = profileId;
    return this;
  }

  build() {
    super.build();

    this.body = new EnumProfileDeactivationBody(this.enumprofiledeactivation);
    this.message = new EnumProfileDeactivationMessage(this.header, this.body);

    return new Message({"message":this.message}, MessageTypeEnum.enumprofiledeactivation);
  }
}

module.exports = EnumProfileDeactivationBuilder;
