const Message = require('../message');
const MessageTypeEnum = require('../messagetype');
const MessageBuilder = require('./MessageBuilder');
const EnumNumberSequenceBuilder = require('./EnumNumberSequenceBuilder');
const EnumDeactivationNumberMessage = require('../model/EnumDeactivationNumberMessage');
const EnumDeactivationNumberBody = require('../model/EnumDeactivationNumberBody');
const EnumContent = require('../model/EnumContent');

class EnumDeactivationNumberBuilder extends MessageBuilder {

  constructor() {
    super();
    this.enumcontent = new EnumContent();
    this.enumcontent.repeats = [];
  }

  setDossierId(dossierId) {
    this.enumcontent.dossierid = dossierId;
    return this;
  }

  setCurrentNetworkOperator(currentNetworkOperator) {
    this.enumcontent.currentnetworkoperator = currentNetworkOperator;
    return this;
  }

  setTypeOfNumber(typeOfNumber) {
    this.enumcontent.typeofnumber = typeOfNumber;
    return this;
  }

  addEnumNumberSequence() {
    return new EnumNumberSequenceBuilder(this);
  }

  build() {
    super.build();
    this.body = new EnumDeactivationNumberBody(this.enumcontent);
    this.message = new EnumDeactivationNumberMessage(this.header, this.body);

    return new Message({"message":this.message}, MessageTypeEnum.enumdeactivationnumber);
  }
}

module.exports = EnumDeactivationNumberBuilder;


