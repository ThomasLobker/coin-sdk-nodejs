const Sender = require('../model/Sender');
const Receiver = require('../model/Receiver');
const Header = require('../model/Header');

class HeaderBuilder {

  setHeader(senderNetworkOperator, receiverNetworkOperator) {
    this.senderNetworkOperator = senderNetworkOperator;
    this.receiverNetworkOperator = receiverNetworkOperator;
    return this;
  }

  setFullHeader(senderNetworkOperator, senderServiceProvider, receiverNetworkOperator, receiverServiceProvider) {
    this.senderNetworkOperator = senderNetworkOperator;
    this.senderServiceProvider = senderServiceProvider;

    this.receiverNetworkOperator = receiverNetworkOperator;
    this.receiverServiceProvider = receiverServiceProvider;
    return this;
  }

  build() {
    if (this.senderServiceProvider) {
      this.sender = Sender.constructFromObject({"networkoperator": this.senderNetworkOperator, "serviceprovider": this.senderServiceProvider});
    } else {
      this.sender = new Sender(this.senderNetworkOperator);
    }

    if (this.receiverServiceProvider) {
      this.receiver = Receiver.constructFromObject({"networkoperator": this.receiverNetworkOperator, "serviceprovider": this.receiverServiceProvider});
    } else {
      this.receiver = new Receiver(this.receiverNetworkOperator);
    }

    const today = new Date();
    const timestamp = today.getFullYear() +
      ("0" + (today.getMonth() + 1)).slice(-2) +
      ("0" + today.getDate()).slice(-2) +
      ("0" + today.getHours() + 1 ).slice(-2) +
      ("0" + today.getMinutes()).slice(-2) +
      ("0" + today.getSeconds()).slice(-2);

    this.header = new Header(
      this.receiver,
      this.sender,
      timestamp);
  }
}

module.exports = HeaderBuilder;
