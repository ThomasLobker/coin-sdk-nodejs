const TariffChangeServiceNumberSeq = require('../model/TariffChangeServiceNumberSeq');
const TariffChangeServiceNumberRepeats = require('../model/TariffChangeServiceNumberRepeats');
const TariffInfo = require('../model/TariffInfo');
const NumberSeries = require('../model/NumberSeries');

class PortingRequestSequenceBuilder {

  constructor(parent) {
    this.parent = parent;
    this.tariffchangesnseq = new TariffChangeServiceNumberSeq();
  }

  setNumberSeries(start, end) {
    const numberSeries = new NumberSeries();
    numberSeries.start = start;
    numberSeries.end = end;
    this.tariffchangesnseq.numberseries = numberSeries;

    return this;
  }

  setTariffInfo(peak, offpeak, currency, type, vat) {
    this.tariffchangesnseq.tariffinfonew = new TariffInfo();
    this.tariffchangesnseq.tariffinfonew.peak = peak;
    this.tariffchangesnseq.tariffinfonew.offpeak = offpeak;
    this.tariffchangesnseq.tariffinfonew.currency = currency;
    this.tariffchangesnseq.tariffinfonew.type = type;
    this.tariffchangesnseq.tariffinfonew.vat = vat;

    return this;
  }


  finish() {
    this.parent.tariffchangesn.repeats.push(new TariffChangeServiceNumberRepeats(this.tariffchangesnseq));
    return this.parent;
  }
}

module.exports = PortingRequestSequenceBuilder;


