const Message = require('../message');
const MessageTypeEnum = require('../messagetype');
const MessageBuilder = require('./MessageBuilder');
const DeactivationServiceNumberSequenceBuilder = require('./DeactivationServiceNumberSequenceBuilder');
const DeactivationServiceNumberMessage = require('../model/DeactivationServiceNumberMessage');
const DeactivationServiceNumberBody = require('../model/DeactivationServiceNumberBody');
const DeactivationServiceNumber = require('../model/DeactivationServiceNumber');

class DeactivationServiceNumberBuilder extends MessageBuilder {

  constructor() {
    super();
    this.deactivationservicenumber = new DeactivationServiceNumber();
    this.deactivationservicenumber.repeats = [];
  }

  setDossierId(dossierId) {
    this.deactivationservicenumber.dossierid = dossierId;
    return this;
  }

  setPlannedDateTime(plannedDateTime) {
    this.deactivationservicenumber.planneddatetime = plannedDateTime;
    return this;
  }

  setPlatformProvider(platformProvider) {
    this.deactivationservicenumber.platformprovider = platformProvider;
    return this;
  }

  addDeactivationServiceNumberSequence() {
    return new DeactivationServiceNumberSequenceBuilder(this);
  }

  build() {
    super.build();
    this.body = new DeactivationServiceNumberBody(this.deactivationservicenumber);
    this.message = new DeactivationServiceNumberMessage(this.header, this.body);

    return new Message({"message":this.message}, MessageTypeEnum.deactivationsn);
  }
}

module.exports = DeactivationServiceNumberBuilder;


