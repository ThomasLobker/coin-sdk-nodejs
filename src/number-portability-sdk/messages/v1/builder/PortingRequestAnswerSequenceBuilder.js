const PortingRequestAnswerRepeats = require('../model/PortingPerformedRepeats');
const PortingRequestAnswerSeq = require('../model/PortingPerformedSeq');
const NumberSeries = require('../model/NumberSeries');

class PortingRequestAnswerSequenceBuilder {

  constructor(parent) {
    this.parent = parent;
    this.portingrequestanswerseq = new PortingRequestAnswerSeq();
  }

  setNumberSeries(start, end) {
    const numberSeries = new NumberSeries();
    numberSeries.start = start;
    numberSeries.end = end;
    this.portingrequestanswerseq.numberseries = numberSeries;

    return this;
  }

  setBlockingCode(blockingCode) {
    this.portingrequestanswerseq.blockingcode = blockingCode;
    return this;
  }

  setFirstPossibleDate(firstPossibleDate) {
    this.portingrequestanswerseq.firstpossibledate = firstPossibleDate;
    return this;
  }

  setDonorNetworkOperator(donorNetworkOperator) {
    this.portingrequestanswerseq.donorNetworkOperator = donorNetworkOperator;
    return this;
  }

  setDonorServiceProvider(donorServiceProvider) {
    this.portingrequestanswerseq.donorServiceProvider = donorServiceProvider;
    return this;
  }

  setNote(note) {
    this.portingrequestanswerseq.note = note;
    return this;
  }

  finish() {
    this.parent.portingrequestanswer.repeats.push(new PortingRequestAnswerRepeats(this.portingrequestanswerseq));
    return this.parent;
  }
}

module.exports = PortingRequestAnswerSequenceBuilder;


