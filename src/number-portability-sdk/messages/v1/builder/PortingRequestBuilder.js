const Message = require('../message');
const MessageTypeEnum = require('../messagetype');
const MessageBuilder = require('./MessageBuilder');
const PortingRequestMessage = require('../model/PortingRequestMessage');
const PortingRequestBody = require('../model/PortingRequestBody');
const PortingRequest = require('../model/PortingRequest');
const CustomerInfo = require('../model/CustomerInfo');
const PortingRequestSequenceBuilder = require('./PortingRequestSequenceBuilder');

class PortingRequestBuilder extends MessageBuilder {

  constructor() {
    super();
    this.portingrequest = new PortingRequest();
    this.portingrequest.repeats = [];
  }

  setDossierId(dossierId) {
    this.portingrequest.dossierid = dossierId;
    return this;
  }

  setRecipientNetworkOperator(recipientNetworkOperator) {
    this.portingrequest.recipientnetworkoperator = recipientNetworkOperator;
    return this;
  }

  setRecipientServiceProvider(recipientServiceProvider) {
    this.portingrequest.recipientserviceprovider = recipientServiceProvider;
    return this;
  }

  setCustomerInfo(lastname, companyname, housenr, housenrext, postcode, customerid) {
    this.portingrequest.customerinfo = new CustomerInfo();
    this.portingrequest.customerinfo.lastname = lastname;
    this.portingrequest.customerinfo.companyname = companyname;
    this.portingrequest.customerinfo.housenr = housenr;
    this.portingrequest.customerinfo.housenrext = housenrext;
    this.portingrequest.customerinfo.postcode = postcode;
    this.portingrequest.customerinfo.customerid = customerid;

    return this;
  }

  addPortingRequestSequence() {
    return new PortingRequestSequenceBuilder(this);
  }

  build() {
    super.build();

    this.body = new PortingRequestBody(this.portingrequest);
    this.message = new PortingRequestMessage(this.header, this.body);

    return new Message({"message":this.message}, MessageTypeEnum.portingrequest);
  }
}

module.exports = PortingRequestBuilder;
