const Message = require('../message');
const MessageTypeEnum = require('../messagetype');
const MessageBuilder = require('./MessageBuilder');
const EnumNumberSequenceBuilder = require('./EnumNumberSequenceBuilder');
const EnumDeactivationRangeMessage = require('../model/EnumDeactivationRangeMessage');
const EnumDeactivationRangeBody = require('../model/EnumDeactivationRangeBody');
const EnumContent = require('../model/EnumContent');

class EnumDeactivationRangeBuilder extends MessageBuilder {

  constructor() {
    super();
    this.enumcontent = new EnumContent();
    this.enumcontent.repeats = [];
  }

  setDossierId(dossierId) {
    this.enumcontent.dossierid = dossierId;
    return this;
  }

  setCurrentNetworkOperator(currentNetworkOperator) {
    this.enumcontent.currentnetworkoperator = currentNetworkOperator;
    return this;
  }

  setTypeOfNumber(typeOfNumber) {
    this.enumcontent.typeofnumber = typeOfNumber;
    return this;
  }

  addEnumNumberSequence() {
    return new EnumNumberSequenceBuilder(this);
  }

  build() {
    super.build();
    this.body = new EnumDeactivationRangeBody(this.enumcontent);
    this.message = new EnumDeactivationRangeMessage(this.header, this.body);

    return new Message({"message":this.message}, MessageTypeEnum.enumdeactivationrange);
  }
}

module.exports = EnumDeactivationRangeBuilder;


