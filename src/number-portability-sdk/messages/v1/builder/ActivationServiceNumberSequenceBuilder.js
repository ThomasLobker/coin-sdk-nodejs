const ActivationServiceNumberRepeats = require('../model/ActivationServiceNumberRepeats');
const ActivationServiceNumberSeq = require('../model/ActivationServiceNumberSeq');
const NumberSeries = require('../model/NumberSeries');
const TariffInfo = require('../model/TariffInfo');

class ActivationServiceNumberSequenceBuilder {

  constructor(parent) {
    this.parent = parent;
    this.activationServiceNumberSequence = new ActivationServiceNumberSeq();
  }

  setNumberSeries(start, end) {
    const numberSeries = new NumberSeries();
    numberSeries.start = start;
    numberSeries.end = end;
    this.activationServiceNumberSequence.numberseries = numberSeries;

    return this;
  }

  setTariffinfo(peak, offPeak, currency, type, vat) {
    const tariffInfo = new TariffInfo();
    tariffInfo.peak = peak;
    tariffInfo.offpeak = offPeak;
    tariffInfo.currency = currency;
    tariffInfo.type = type;
    tariffInfo.vat = vat;

    this.activationServiceNumberSequence.tariffinfo = tariffInfo;

    return this;
  }

  setPop(pop) {
    this.activationServiceNumberSequence.pop = pop;
    return this;
  }

  finish() {
    this.parent.activationservicenumber.repeats.push(new ActivationServiceNumberRepeats(this.activationServiceNumberSequence));
    return this.parent;
  }
}

module.exports = ActivationServiceNumberSequenceBuilder;
