const Message = require('../message');
const MessageTypeEnum = require('../messagetype');
const MessageBuilder = require('./MessageBuilder');
const PortingRequestAnswerMessage = require('../model/PortingRequestAnswerMessage');
const PortingRequestAnswerBody = require('../model/PortingRequestAnswerBody');
const PortingRequestAnswer = require('../model/PortingRequestAnswer');
const PortingRequestAnswerSequenceBuilder = require('./PortingRequestAnswerSequenceBuilder');

class PortingRequestAnswerBuilder extends MessageBuilder {

  constructor() {
    super();
    this.portingrequestanswer = new PortingRequestAnswer();
    this.portingrequestanswer.repeats = [];
  }

  setDossierId(dossierId) {
    this.portingrequestanswer.dossierid = dossierId;
    return this;
  }

  setBlocking(blocking) {
    this.portingrequestanswer.blocking = blocking;
    return this;
  }

  addPortingRequestAnswerSequence() {
    return new PortingRequestAnswerSequenceBuilder(this);
  }

  build() {
    super.build();

    this.body = new PortingRequestAnswerBody(this.portingrequestanswer);
    this.message = new PortingRequestAnswerMessage(this.header, this.body);

    return new Message({"message":this.message}, MessageTypeEnum.portingrequestanswer);
  }
}

module.exports = PortingRequestAnswerBuilder;
