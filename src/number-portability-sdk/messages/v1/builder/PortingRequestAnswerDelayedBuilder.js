const Message = require('../message');
const MessageTypeEnum = require('../messagetype');
const MessageBuilder = require('./MessageBuilder');
const PortingRequestAnswerDelayedMessage = require('../model/PortingRequestAnswerDelayedMessage');
const PortingRequestAnswerDelayedBody = require('../model/PortingRequestAnswerDelayedBody');
const PortingRequestAnswerDelayed = require('../model/PortingRequestAnswerDelayed');

class PortingRequestAnswerDelayedBuilder extends MessageBuilder {

  constructor() {
    super();
    this.portingrequestanswerdelayed = new PortingRequestAnswerDelayed();
  }

  setDossierId(dossierId) {
    this.portingrequestanswerdelayed.dossierid = dossierId;
    return this;
  }

  setDonorNetworkOperator(donorNetworkOperator) {
    this.portingrequestanswerdelayed.donornetworkoperator = donorNetworkOperator;
    return this;
  }

  setDonorServiceProvider(donorServiceProvider) {
    this.portingrequestanswerdelayed.donorserviceprovider = donorServiceProvider;
    return this;
  }

  setAnswerDueDateTime(answerDueDateTime) {
    this.portingrequestanswerdelayed.answerduedatetime = answerDueDateTime;
    return this;
  }

  setReasonCode(reasonCode) {
    this.portingrequestanswerdelayed.reasoncode = reasonCode;
    return this;
  }

  build() {
    super.build();
    this.body = new PortingRequestAnswerDelayedBody(this.portingrequestanswerdelayed);
    this.message = new PortingRequestAnswerDelayedMessage(this.header, this.body);
    return new Message({"message":this.message}, MessageTypeEnum.pradelayed);
  }
}

module.exports = PortingRequestAnswerDelayedBuilder;
