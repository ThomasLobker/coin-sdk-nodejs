const PortingRequestRepeats = require('../model/PortingPerformedRepeats');
const PortingRequestSeq = require('../model/PortingPerformedSeq');
const NumberSeries = require('../model/NumberSeries');
const EnumRepeatsBuilder = require('./EnumRepeatsBuilder');

class PortingRequestSequenceBuilder {

  constructor(parent) {
    this.parent = parent;
    this.portingrequestseq = new PortingRequestSeq();
  }

  setNumberSeries(start, end) {
    const numberSeries = new NumberSeries();
    numberSeries.start = start;
    numberSeries.end = end;
    this.portingrequestseq.numberseries = numberSeries;

    return this;
  }

  setProfileIds(profileIds) {
    const enumRepeats = new EnumRepeatsBuilder();
    enumRepeats.setProfileIds(profileIds);
    this.portingrequestseq.repeats = enumRepeats.build();
    return this;
  }

  finish() {
    this.parent.portingrequest.repeats.push(new PortingRequestRepeats(this.portingrequestseq));
    return this.parent;
  }
}

module.exports = PortingRequestSequenceBuilder;


