const Message = require('../message');
const MessageTypeEnum = require('../messagetype');
const MessageBuilder = require('./MessageBuilder');
const EnumOperatorSequenceBuilder = require('./EnumOperatorSequenceBuilder');
const EnumDeactivationOperatorMessage = require('../model/EnumDeactivationOperatorMessage');
const EnumDeactivationOperatorBody = require('../model/EnumDeactivationOperatorBody');
const EnumOperatorContent = require('../model/EnumOperatorContent');

class EnumDeactivationOperatorBuilder extends MessageBuilder {

  constructor() {
    super();
    this.enumcontent = new EnumOperatorContent();
    this.enumcontent.repeats = [];
  }

  setDossierId(dossierId) {
    this.enumcontent.dossierid = dossierId;
    return this;
  }

  setCurrentNetworkOperator(currentNetworkOperator) {
    this.enumcontent.currentnetworkoperator = currentNetworkOperator;
    return this;
  }

  setTypeOfNumber(typeOfNumber) {
    this.enumcontent.typeofnumber = typeOfNumber;
    return this;
  }

  addEnumOperatorSequence() {
    return new EnumOperatorSequenceBuilder(this);
  }

  build() {
    super.build();
    this.body = new EnumDeactivationOperatorBody(this.enumcontent);
    this.message = new EnumDeactivationOperatorMessage(this.header, this.body);

    return new Message({"message":this.message}, MessageTypeEnum.enumdeactivationoperator);
  }
}

module.exports = EnumDeactivationOperatorBuilder;


