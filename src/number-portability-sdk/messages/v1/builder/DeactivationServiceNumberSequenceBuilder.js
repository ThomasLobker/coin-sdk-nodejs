const DeactivationServiceNumberRepeats = require('../model/DeactivationServiceNumberRepeats');
const DeactivationServiceNumberSeq = require('../model/DeactivationServiceNumberSeq');
const NumberSeries = require('../model/NumberSeries');

class DeactivationServiceNumberSequenceBuilder {

  constructor(parent) {
    this.parent = parent;
    this.deactivationServiceNumberSequence = new DeactivationServiceNumberSeq();
  }

  setNumberSeries(start, end) {
    const numberSeries = new NumberSeries();
    numberSeries.start = start;
    numberSeries.end = end;
    this.deactivationServiceNumberSequence.numberseries = numberSeries;

    return this;
  }

  setPop(pop) {
    this.deactivationServiceNumberSequence.pop = pop;
    return this;
  }

  finish() {
    this.parent.deactivationservicenumber.repeats.push(new DeactivationServiceNumberRepeats(this.deactivationServiceNumberSequence));
    return this.parent;
  }
}

module.exports = DeactivationServiceNumberSequenceBuilder;


