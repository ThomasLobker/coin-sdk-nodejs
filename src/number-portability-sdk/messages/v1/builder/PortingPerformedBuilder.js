const Message = require('../message');
const MessageTypeEnum = require('../messagetype');
const MessageBuilder = require('./MessageBuilder');
const PortingPerformedMessage = require('../model/PortingPerformedMessage');
const PortingPerformedBody = require('../model/PortingPerformedBody');
const PortingPerformed = require('../model/PortingPerformed');
const PortingPerformedSequenceBuilder = require('./PortingPerformedSequenceBuilder');

class PortingPerformedBuilder extends MessageBuilder {

  constructor() {
    super();
    this.portingPerformed = new PortingPerformed();
    this.portingPerformed.repeats = [];
  }

  setDossierId(dossierId) {
    this.portingPerformed.dossierid = dossierId;
    return this;
  }

  setRecipientNetworkOperator(recipientNetworkOperator) {
    this.portingPerformed.recipientnetworkoperator = recipientNetworkOperator;
    return this;
  }

  setDonorNetworkOperator(donorNetworkOperator) {
    this.portingPerformed.donornetworkoperator = donorNetworkOperator;
    return this;
  }

  setActualDateTime(actualDateTime) {
    this.portingPerformed.actualdatetime = actualDateTime;
    return this;
  }

  setBatchPorting(batchPorting) {
    this.portingPerformed.batchporting = batchPorting;
    return this;
  }

  addPortingPerformedSequence() {
    return new PortingPerformedSequenceBuilder(this);
  }

  build() {
    super.build();

    this.body = new PortingPerformedBody(this.portingPerformed);
    this.message = new PortingPerformedMessage(this.header, this.body);

    return new Message({"message":this.message}, MessageTypeEnum.portingperformed);
  }
}

module.exports = PortingPerformedBuilder;
