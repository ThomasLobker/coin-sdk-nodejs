const PortingPerformedRepeats = require('../model/PortingPerformedRepeats');
const PortingPerformedSeq = require('../model/PortingPerformedSeq');
const NumberSeries = require('../model/NumberSeries');
const EnumRepeatsBuilder = require('./EnumRepeatsBuilder');

class PortingPerformedSequenceBuilder {

  constructor(parent) {
    this.parent = parent;
    this.portingperformedseq = new PortingPerformedSeq();
  }

  setNumberSeries(start, end) {
    const numberSeries = new NumberSeries();
    numberSeries.start = start;
    numberSeries.end = end;
    this.portingperformedseq.numberseries = numberSeries;

    return this;
  }

  setBackPorting(backPorting) {
    this.portingperformedseq.backporting = backPorting;
    return this;
  }

  setPop(pop) {
    this.portingperformedseq.pop = pop;
    return this;
  }

  setProfileIds(profileIds) {
    const enumRepeats = new EnumRepeatsBuilder();
    enumRepeats.setProfileIds(profileIds);
    this.portingperformedseq.repeats = enumRepeats.build();
    return this;
  }

  finish() {
    this.parent.portingPerformed.repeats.push(new PortingPerformedRepeats(this.portingperformedseq));
    return this.parent;
  }
}

module.exports = PortingPerformedSequenceBuilder;


