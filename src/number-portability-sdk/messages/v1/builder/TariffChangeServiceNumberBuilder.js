const Message = require('../message');
const MessageTypeEnum = require('../messagetype');
const MessageBuilder = require('./MessageBuilder');
const TariffChangeServiceNumber = require('../model/TariffChangeServiceNumber');
const TariffChangeServiceNumberBody = require('../model/TariffChangeServiceNumberBody');
const TariffChangeServiceNumberMessage = require('../model/TariffChangeServiceNumberMessage');
const TariffChangeServiceNumberSequenceBuilder = require('./TariffChangeServiceNumberSequenceBuilder');

class TariffChangeServiceNumberBuilder extends MessageBuilder {

  constructor() {
    super();
    this.tariffchangesn = new TariffChangeServiceNumber();
    this.tariffchangesn.repeats = [];
  }

  setDossierId(dossierId) {
    this.tariffchangesn.dossierid = dossierId;
    return this;
  }

  setPlatformProvider(platformProvider) {
    this.tariffchangesn.platformprovider = platformProvider;
    return this;
  }

  setPlannedDateTime(plannedDateTime) {
    this.tariffchangesn.planneddatetime = plannedDateTime;
    return this;
  }

  addTariffChangeServiceNumberSequence() {
    return new TariffChangeServiceNumberSequenceBuilder(this);
  }

  build() {
    super.build();

    this.body = new TariffChangeServiceNumberBody(this.tariffchangesn);
    this.message = new TariffChangeServiceNumberMessage(this.header, this.body);

    return new Message({"message":this.message}, MessageTypeEnum.tariffchangesn);
  }
}

module.exports = TariffChangeServiceNumberBuilder;
