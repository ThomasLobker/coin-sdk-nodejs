const EnumProfileSeq = require('../model/EnumProfileSeq');
const EnumRepeats = require('../model/EnumRepeats');

class EnumRepeatsBuilder {

  setProfileIds(profileIds) {
    this.profileIds = profileIds;
    return this;
  }

  build() {
    const enumRepeats = [];
    this.profileIds.forEach(function(item) {
      const enumProfileSeq = new EnumProfileSeq();
      enumProfileSeq.profileid = item;
      enumRepeats.push(new EnumRepeats(enumProfileSeq));
    });
    return enumRepeats;
  }
}

module.exports = EnumRepeatsBuilder;


