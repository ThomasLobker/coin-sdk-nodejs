const MessageTypeEnum = {
  "activationsn": "activationsn",
  "cancel": "cancel",
  "deactivation": "deactivation",
  "deactivationsn": "deactivationsn",
  "enumactivationnumber": "enumactivationnumber",
  "enumactivationoperator": "enumactivationoperator",
  "enumactivationrange": "enumactivationrange",
  "enumdeactivationnumber": "enumdeactivationnumber",
  "enumdeactivationoperator": "enumdeactivationoperator",
  "enumdeactivationrange": "enumdeactivationrange",
  "enumprofileactivation": "enumprofileactivation",
  "enumprofiledeactivation": "enumprofiledeactivation",
  "portingperformed": "portingperformed",
  "portingrequest": "portingrequest",
  "portingrequestanswer": "portingrequestanswer",
  "portingrequestanswerdelayed": "portingrequestanswerdelayed",
  "rangeactivation": "rangeactivation",
  "rangedeactivation": "rangedeactivation",
  "tariffchangesn": "tariffchangesn"
};

module.exports = MessageTypeEnum;
