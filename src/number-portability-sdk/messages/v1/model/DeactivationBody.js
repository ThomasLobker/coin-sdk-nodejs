/**
 * COIN CRDB Rest API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0
 * Contact: servicedesk@coin.nl
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/Deactivation'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./Deactivation'));
  } else {
    // Browser globals (root is window)
    if (!root.CoinCrdbRestApi) {
      root.CoinCrdbRestApi = {};
    }
    root.CoinCrdbRestApi.DeactivationBody = factory(root.CoinCrdbRestApi.ApiClient, root.CoinCrdbRestApi.Deactivation);
  }
}(this, function(ApiClient, Deactivation) {
  'use strict';




  /**
   * The DeactivationBody model module.
   * @module model/DeactivationBody
   * @version 1.0.0
   */

  /**
   * Constructs a new <code>DeactivationBody</code>.
   * @alias module:model/DeactivationBody
   * @class
   * @param deactivation {module:model/Deactivation} 
   */
  var exports = function(deactivation) {
    var _this = this;

    _this['deactivation'] = deactivation;
  };

  /**
   * Constructs a <code>DeactivationBody</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/DeactivationBody} obj Optional instance to populate.
   * @return {module:model/DeactivationBody} The populated <code>DeactivationBody</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('deactivation')) {
        obj['deactivation'] = Deactivation.constructFromObject(data['deactivation']);
      }
    }
    return obj;
  };

  /**
   * @member {module:model/Deactivation} deactivation
   */
  exports.prototype['deactivation'] = undefined;



  return exports;
}));


