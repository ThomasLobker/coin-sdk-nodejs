/**
 * COIN CRDB Rest API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0
 * Contact: servicedesk@coin.nl
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/PortingPerformed'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./PortingPerformed'));
  } else {
    // Browser globals (root is window)
    if (!root.CoinCrdbRestApi) {
      root.CoinCrdbRestApi = {};
    }
    root.CoinCrdbRestApi.PortingPerformedBody = factory(root.CoinCrdbRestApi.ApiClient, root.CoinCrdbRestApi.PortingPerformed);
  }
}(this, function(ApiClient, PortingPerformed) {
  'use strict';




  /**
   * The PortingPerformedBody model module.
   * @module model/PortingPerformedBody
   * @version 1.0.0
   */

  /**
   * Constructs a new <code>PortingPerformedBody</code>.
   * @alias module:model/PortingPerformedBody
   * @class
   * @param portingperformed {module:model/PortingPerformed} 
   */
  var exports = function(portingperformed) {
    var _this = this;

    _this['portingperformed'] = portingperformed;
  };

  /**
   * Constructs a <code>PortingPerformedBody</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/PortingPerformedBody} obj Optional instance to populate.
   * @return {module:model/PortingPerformedBody} The populated <code>PortingPerformedBody</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('portingperformed')) {
        obj['portingperformed'] = PortingPerformed.constructFromObject(data['portingperformed']);
      }
    }
    return obj;
  };

  /**
   * @member {module:model/PortingPerformed} portingperformed
   */
  exports.prototype['portingperformed'] = undefined;



  return exports;
}));


