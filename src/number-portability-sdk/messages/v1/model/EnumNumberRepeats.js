/**
 * COIN CRDB Rest API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0
 * Contact: servicedesk@coin.nl
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/EnumNumberSeq'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./EnumNumberSeq'));
  } else {
    // Browser globals (root is window)
    if (!root.CoinCrdbRestApi) {
      root.CoinCrdbRestApi = {};
    }
    root.CoinCrdbRestApi.EnumNumberRepeats = factory(root.CoinCrdbRestApi.ApiClient, root.CoinCrdbRestApi.EnumNumberSeq);
  }
}(this, function(ApiClient, EnumNumberSeq) {
  'use strict';




  /**
   * The EnumNumberRepeats model module.
   * @module model/EnumNumberRepeats
   * @version 1.0.0
   */

  /**
   * Constructs a new <code>EnumNumberRepeats</code>.
   * @alias module:model/EnumNumberRepeats
   * @class
   * @param seq {module:model/EnumNumberSeq} 
   */
  var exports = function(seq) {
    var _this = this;

    _this['seq'] = seq;
  };

  /**
   * Constructs a <code>EnumNumberRepeats</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/EnumNumberRepeats} obj Optional instance to populate.
   * @return {module:model/EnumNumberRepeats} The populated <code>EnumNumberRepeats</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('seq')) {
        obj['seq'] = EnumNumberSeq.constructFromObject(data['seq']);
      }
    }
    return obj;
  };

  /**
   * @member {module:model/EnumNumberSeq} seq
   */
  exports.prototype['seq'] = undefined;



  return exports;
}));


