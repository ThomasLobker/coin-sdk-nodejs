/**
 * COIN CRDB Rest API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0
 * Contact: servicedesk@coin.nl
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/Receiver', 'model/Sender'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./Receiver'), require('./Sender'));
  } else {
    // Browser globals (root is window)
    if (!root.CoinCrdbRestApi) {
      root.CoinCrdbRestApi = {};
    }
    root.CoinCrdbRestApi.Header = factory(root.CoinCrdbRestApi.ApiClient, root.CoinCrdbRestApi.Receiver, root.CoinCrdbRestApi.Sender);
  }
}(this, function(ApiClient, Receiver, Sender) {
  'use strict';




  /**
   * The Header model module.
   * @module model/Header
   * @version 1.0.0
   */

  /**
   * Constructs a new <code>Header</code>.
   * @alias module:model/Header
   * @class
   * @param receiver {module:model/Receiver} 
   * @param sender {module:model/Sender} 
   * @param timestamp {String} 
   */
  var exports = function(receiver, sender, timestamp) {
    var _this = this;

    _this['receiver'] = receiver;
    _this['sender'] = sender;
    _this['timestamp'] = timestamp;
  };

  /**
   * Constructs a <code>Header</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/Header} obj Optional instance to populate.
   * @return {module:model/Header} The populated <code>Header</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('receiver')) {
        obj['receiver'] = Receiver.constructFromObject(data['receiver']);
      }
      if (data.hasOwnProperty('sender')) {
        obj['sender'] = Sender.constructFromObject(data['sender']);
      }
      if (data.hasOwnProperty('timestamp')) {
        obj['timestamp'] = ApiClient.convertToType(data['timestamp'], 'String');
      }
    }
    return obj;
  };

  /**
   * @member {module:model/Receiver} receiver
   */
  exports.prototype['receiver'] = undefined;
  /**
   * @member {module:model/Sender} sender
   */
  exports.prototype['sender'] = undefined;
  /**
   * @member {String} timestamp
   */
  exports.prototype['timestamp'] = undefined;



  return exports;
}));


