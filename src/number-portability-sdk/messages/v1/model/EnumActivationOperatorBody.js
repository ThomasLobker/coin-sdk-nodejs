/**
 * COIN CRDB Rest API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0
 * Contact: servicedesk@coin.nl
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/EnumOperatorContent'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./EnumOperatorContent'));
  } else {
    // Browser globals (root is window)
    if (!root.CoinCrdbRestApi) {
      root.CoinCrdbRestApi = {};
    }
    root.CoinCrdbRestApi.EnumActivationOperatorBody = factory(root.CoinCrdbRestApi.ApiClient, root.CoinCrdbRestApi.EnumOperatorContent);
  }
}(this, function(ApiClient, EnumOperatorContent) {
  'use strict';




  /**
   * The EnumActivationOperatorBody model module.
   * @module model/EnumActivationOperatorBody
   * @version 1.0.0
   */

  /**
   * Constructs a new <code>EnumActivationOperatorBody</code>.
   * @alias module:model/EnumActivationOperatorBody
   * @class
   * @param enumactivationoperator {module:model/EnumOperatorContent} 
   */
  var exports = function(enumactivationoperator) {
    var _this = this;

    _this['enumactivationoperator'] = enumactivationoperator;
  };

  /**
   * Constructs a <code>EnumActivationOperatorBody</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/EnumActivationOperatorBody} obj Optional instance to populate.
   * @return {module:model/EnumActivationOperatorBody} The populated <code>EnumActivationOperatorBody</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('enumactivationoperator')) {
        obj['enumactivationoperator'] = EnumOperatorContent.constructFromObject(data['enumactivationoperator']);
      }
    }
    return obj;
  };

  /**
   * @member {module:model/EnumOperatorContent} enumactivationoperator
   */
  exports.prototype['enumactivationoperator'] = undefined;



  return exports;
}));


