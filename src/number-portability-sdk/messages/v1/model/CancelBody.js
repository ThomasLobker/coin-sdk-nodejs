/**
 * COIN CRDB Rest API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0
 * Contact: servicedesk@coin.nl
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/Cancel'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./Cancel'));
  } else {
    // Browser globals (root is window)
    if (!root.CoinCrdbRestApi) {
      root.CoinCrdbRestApi = {};
    }
    root.CoinCrdbRestApi.CancelBody = factory(root.CoinCrdbRestApi.ApiClient, root.CoinCrdbRestApi.Cancel);
  }
}(this, function(ApiClient, Cancel) {
  'use strict';




  /**
   * The CancelBody model module.
   * @module model/CancelBody
   * @version 1.0.0
   */

  /**
   * Constructs a new <code>CancelBody</code>.
   * @alias module:model/CancelBody
   * @class
   * @param cancel {module:model/Cancel} 
   */
  var exports = function(cancel) {
    var _this = this;

    _this['cancel'] = cancel;
  };

  /**
   * Constructs a <code>CancelBody</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/CancelBody} obj Optional instance to populate.
   * @return {module:model/CancelBody} The populated <code>CancelBody</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('cancel')) {
        obj['cancel'] = Cancel.constructFromObject(data['cancel']);
      }
    }
    return obj;
  };

  /**
   * @member {module:model/Cancel} cancel
   */
  exports.prototype['cancel'] = undefined;



  return exports;
}));


