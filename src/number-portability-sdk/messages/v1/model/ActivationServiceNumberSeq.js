/**
 * COIN CRDB Rest API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0
 * Contact: servicedesk@coin.nl
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/NumberSeries', 'model/TariffInfo'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./NumberSeries'), require('./TariffInfo'));
  } else {
    // Browser globals (root is window)
    if (!root.CoinCrdbRestApi) {
      root.CoinCrdbRestApi = {};
    }
    root.CoinCrdbRestApi.ActivationServiceNumberSeq = factory(root.CoinCrdbRestApi.ApiClient, root.CoinCrdbRestApi.NumberSeries, root.CoinCrdbRestApi.TariffInfo);
  }
}(this, function(ApiClient, NumberSeries, TariffInfo) {
  'use strict';




  /**
   * The ActivationServiceNumberSeq model module.
   * @module model/ActivationServiceNumberSeq
   * @version 1.0.0
   */

  /**
   * Constructs a new <code>ActivationServiceNumberSeq</code>.
   * @alias module:model/ActivationServiceNumberSeq
   * @class
   * @param numberseries {module:model/NumberSeries} 
   * @param tariffinfo {module:model/TariffInfo} 
   * @param pop {String} 
   */
  var exports = function(numberseries, tariffinfo, pop) {
    var _this = this;

    _this['numberseries'] = numberseries;
    _this['tariffinfo'] = tariffinfo;
    _this['pop'] = pop;
  };

  /**
   * Constructs a <code>ActivationServiceNumberSeq</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/ActivationServiceNumberSeq} obj Optional instance to populate.
   * @return {module:model/ActivationServiceNumberSeq} The populated <code>ActivationServiceNumberSeq</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('numberseries')) {
        obj['numberseries'] = NumberSeries.constructFromObject(data['numberseries']);
      }
      if (data.hasOwnProperty('tariffinfo')) {
        obj['tariffinfo'] = TariffInfo.constructFromObject(data['tariffinfo']);
      }
      if (data.hasOwnProperty('pop')) {
        obj['pop'] = ApiClient.convertToType(data['pop'], 'String');
      }
    }
    return obj;
  };

  /**
   * @member {module:model/NumberSeries} numberseries
   */
  exports.prototype['numberseries'] = undefined;
  /**
   * @member {module:model/TariffInfo} tariffinfo
   */
  exports.prototype['tariffinfo'] = undefined;
  /**
   * @member {String} pop
   */
  exports.prototype['pop'] = undefined;



  return exports;
}));


