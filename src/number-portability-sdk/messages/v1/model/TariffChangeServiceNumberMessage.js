/**
 * COIN CRDB Rest API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0
 * Contact: servicedesk@coin.nl
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/Header', 'model/TariffChangeServiceNumberBody'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./Header'), require('./TariffChangeServiceNumberBody'));
  } else {
    // Browser globals (root is window)
    if (!root.CoinCrdbRestApi) {
      root.CoinCrdbRestApi = {};
    }
    root.CoinCrdbRestApi.TariffChangeServiceNumberMessage = factory(root.CoinCrdbRestApi.ApiClient, root.CoinCrdbRestApi.Header, root.CoinCrdbRestApi.TariffChangeServiceNumberBody);
  }
}(this, function(ApiClient, Header, TariffChangeServiceNumberBody) {
  'use strict';




  /**
   * The TariffChangeServiceNumberMessage model module.
   * @module model/TariffChangeServiceNumberMessage
   * @version 1.0.0
   */

  /**
   * Constructs a new <code>TariffChangeServiceNumberMessage</code>.
   * @alias module:model/TariffChangeServiceNumberMessage
   * @class
   * @param header {module:model/Header} 
   * @param body {module:model/TariffChangeServiceNumberBody} 
   */
  var exports = function(header, body) {
    var _this = this;

    _this['header'] = header;
    _this['body'] = body;
  };

  /**
   * Constructs a <code>TariffChangeServiceNumberMessage</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/TariffChangeServiceNumberMessage} obj Optional instance to populate.
   * @return {module:model/TariffChangeServiceNumberMessage} The populated <code>TariffChangeServiceNumberMessage</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('header')) {
        obj['header'] = Header.constructFromObject(data['header']);
      }
      if (data.hasOwnProperty('body')) {
        obj['body'] = TariffChangeServiceNumberBody.constructFromObject(data['body']);
      }
    }
    return obj;
  };

  /**
   * @member {module:model/Header} header
   */
  exports.prototype['header'] = undefined;
  /**
   * @member {module:model/TariffChangeServiceNumberBody} body
   */
  exports.prototype['body'] = undefined;



  return exports;
}));


