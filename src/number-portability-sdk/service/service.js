const RestApiClient = require('../../common-sdk/rest-api');
const ConfirmationMessage = require('../messages/v1/model/ConfirmationMessage');

const NUMBER_PORTABILITY_URL = '/number-portability/v1';

class NumberPortabilityService extends RestApiClient {

  constructor(consumerName, privateKeyFile = null, encryptedHmacSecretFile = null, validPeriodInSeconds = 30, coinBaseUrl = null) {
    super(consumerName, privateKeyFile, encryptedHmacSecretFile, validPeriodInSeconds);
    this.apiUrl = coinBaseUrl + NUMBER_PORTABILITY_URL;
  }

  sendConfirmation(id) {
    const confirmationMessage = new ConfirmationMessage();
    confirmationMessage.transactionId = id;

    const url = `${this.apiUrl}/dossiers/confirmations/${id}`;

    return this.sendWithToken("PUT", url, confirmationMessage);
  }

  sendMessage(message) {
    return this.postMessage(message.getMessage(), message.getMessageType());
  }

  postMessage(message, messageType) {
    const url = `${this.apiUrl}/dossiers/${messageType}`;
    return this.sendWithToken("POST", url, message);
  }
}

module.exports = NumberPortabilityService;
