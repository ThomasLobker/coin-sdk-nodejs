const utils = require('../../common-sdk/utils');
const EventSource = require('eventsource');
const RestApiClient = require('../../common-sdk/rest-api');
const MessageTypesEnum = require('../messages/v1/messagetype');

class NumberPortabilityMessageConsumer extends RestApiClient {

  constructor(consumerName = null, privateKeyFile = null, encryptedHmacSecretFile = null, coinBaseUrl = null, backOffPeriod = 30, numberOfRetries = 10, validPeriodInSeconds = 30) {
    super(consumerName, privateKeyFile, encryptedHmacSecretFile, validPeriodInSeconds);
    this.sseUri = coinBaseUrl + '/number-portability/v1/dossiers/events';
    this.retryer = utils.createRetryer(numberOfRetries, backOffPeriod, 60000);
  }

  startConsuming(listener, options = {}) {
    const state = {
      deferred: new utils.Deferred(),
      timeoutCheck: utils.createReadTimeoutCheck(),
      sseSource: undefined
    };

    const connectSse = () => {
      const confirmationStatus = options.confirmationStatus || "Unconfirmed";
      const offset = options.offsetPersister && options.offsetPersister.getOffset();
      const url = utils.createUrl(this.sseUri, {offset, confirmationStatus, messageTypes: options.messageTypes});
      const headers = this.security.generateHeaders("GET", url, '', { "User-Agent": RestApiClient.getFullVersion() });
      return new EventSource(url, { headers });
    };

    const setupHandlers = (sseSource, reconnectFn) => {
      sseSource.onerror = () => {
        reconnectFn();
      };
      sseSource.onopen = () => {
        this.retryer.reset();
      };
      sseSource.onmessage = () => { // onmessage is called when an untyped message arrives (currently only keep alives)
        state.timeoutCheck.reset();
        if (listener.onKeepAlive)
          listener.onKeepAlive();
      };
      function handler(event) { // handler is called when a typed message arrives
        state.timeoutCheck.reset();
        const handlerName = listenerEventHandleMap[event.type];
        if (handlerName)
          listener[handlerName](event);
        else if (listener.onUnknownMessage)
          listener.onUnknownMessage(event);
        if (options.offsetPersister)
          options.offsetPersister.persistOffset(event.lastEventId);
      }
      Object.keys(MessageTypesEnum).map(key => sseSource.addEventListener(key+'-v1', handler));
    };

    const reconnect = (error) => {
      state.timeoutCheck.stop();
      state.sseSource.close();
      console.error(`Consumer.getMessages() An error occurred in the server-sent event stream: ${JSON.stringify(error)}`);
      if (!this.retryer.retry(() => connect()))
        state.deferred.reject(error);
    };

    const connect = () => {
      state.sseSource = connectSse();
      setupHandlers(state.sseSource, reconnect);
      state.timeoutCheck.start(reconnect);
    };

    if (options.confirmationStatus && options.confirmationStatus !== "Unconfirmed" && !options.offsetPersister)
      throw new Error("offsetPersister is required in options when confirmationStatus isn't equal to Unconfirmed");
    else
      connect();

    return {
      close: () => {
        if (state.sseSource.readyState === 2) // already closed
          return;
        this.retryer.reset();
        state.timeoutCheck.stop();
        state.sseSource.close();
        state.deferred.resolve();
      },
      promise: state.deferred.promise
    };
  }
}

const listenerEventHandleMap = {
  [MessageTypesEnum.activationsn + "-v1"]: "onActivationsn",
  [MessageTypesEnum.cancel + "-v1"]: "onCancel",
  [MessageTypesEnum.deactivation + "-v1"]: "onDeactivation",
  [MessageTypesEnum.deactivationsn + "-v1"]: "onDeactivationsn",
  [MessageTypesEnum.enumactivationnumber + "-v1"]: "onEnumActivationNumber",
  [MessageTypesEnum.enumactivationoperator + "-v1"]: "onEnumActivationOperator",
  [MessageTypesEnum.enumactivationrange + "-v1"]: "onEnumActivationRange",
  [MessageTypesEnum.enumdeactivationnumber + "-v1"]: "onEnumDeactivationNumber",
  [MessageTypesEnum.enumdeactivationoperator + "-v1"]: "onEnumDeactivationOperator",
  [MessageTypesEnum.enumdeactivationrange + "-v1"]: "onEnumDeactivationRange",
  [MessageTypesEnum.enumprofileactivation + "-v1"]: "onEnumProfileActivation",
  [MessageTypesEnum.enumprofiledeactivation + "-v1"]: "onEnumProfileDeactivation",
  [MessageTypesEnum.portingperformed + "-v1"]: "onPortingPerformed",
  [MessageTypesEnum.portingrequest + "-v1"]: "onPortingRequest",
  [MessageTypesEnum.portingrequestanswer + "-v1"]: "onPortingRequestAnswer",
  [MessageTypesEnum.portingrequestanswerdelayed + "-v1"]: "onPortingRequestAnswerDelayed",
  [MessageTypesEnum.rangeactivation + "-v1"]: "onRangeActivation",
  [MessageTypesEnum.rangedeactivation + "-v1"]: "onRangeDeactivation",
  [MessageTypesEnum.tariffchangesn + "-v1"]: "onTariffChangeSn"
};

module.exports = NumberPortabilityMessageConsumer;
