const MessageTypesEnum = require('../messages/v1/messagetype');



class MessageHandler {

  constructor() {
    this.handlers = [];
    for (const messageTypesKey in MessageTypesEnum) {
      const key = messageTypesKey + "-v1";
      this.handlers[key] = this.onMessage;
    }
  }

  getHandlers() {
    return this.handlers;
  }

  onMessage(event, offsetPersister, listener) {
    switch (event.type) {
      case MessageTypesEnum.activationsn + "-v1": listener.onActivationsn(event); break;
      case MessageTypesEnum.cancel + "-v1": listener.onCancel(event); break;
      case MessageTypesEnum.deactivation + "-v1": listener.onDeactivation(event); break;
      case MessageTypesEnum.deactivationsn + "-v1": listener.onDeactivationsn(event); break;
      case MessageTypesEnum.enumactivationnumber + "-v1": listener.onEnumActivationNumber(event); break;
      case MessageTypesEnum.enumactivationoperator + "-v1": listener.onEnumActivationOperator(event); break;
      case MessageTypesEnum.enumactivationrange + "-v1": listener.onEnumActivationRange(event); break;
      case MessageTypesEnum.enumdeactivationnumber + "-v1": listener.onEnumDeactivationNumber(event); break;
      case MessageTypesEnum.enumdeactivationoperator + "-v1": listener.onEnumDeactivationOperator(event); break;
      case MessageTypesEnum.enumdeactivationrange + "-v1": listener.onEnumDeactivationRange(event); break;
      case MessageTypesEnum.enumprofileactivation + "-v1": listener.onEnumProfileActivation(event); break;
      case MessageTypesEnum.enumprofiledeactivation + "-v1": listener.onEnumProfileDeactivation(event); break;
      case MessageTypesEnum.portingperformed + "-v1": listener.onPortingPerformed(event); break;
      case MessageTypesEnum.portingrequest + "-v1": listener.onPortingRequest(event); break;
      case MessageTypesEnum.portingrequestanswer + "-v1": listener.onPortingRequestAnswer(event); break;
      case MessageTypesEnum.portingrequestanswerdelayed + "-v1": listener.onPortingRequestAnswerDelayed(event); break;
      case MessageTypesEnum.rangeactivation + "-v1": listener.onRangeActivation(event); break;
      case MessageTypesEnum.rangedeactivation + "-v1": listener.onRangeDeactivation(event); break;
      case MessageTypesEnum.tariffchangesn + "-v1": listener.onTariffChangeSn(event); break;

    }
    offsetPersister.persistOffset(event.lastEventId);
  }
}

module.exports = MessageHandler;
