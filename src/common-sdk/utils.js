const fs = require('fs');

class Deferred {
  constructor() {
    this.resolve = null;
    this.reject = null;
    this.promise = new Promise(function(resolve, reject) {
      this.resolve = resolve;
      this.reject = reject;
    }.bind(this));
    Object.freeze(this);
  }
}

module.exports = {
  getFileContent(path) {
    return fs.readFileSync(path, 'UTF-8');
  },

  timestamp() {
    return (new Date()).toJSON().replace(/[T:-]/g, '').split('.')[0];
  },

  createReadTimeoutCheck(maxTimeBetweenData = 40000) {
    let timer;
    let lastMessage = Date.now();
    const scheduleCheck = (reconnectFn) => {
      const delay = Math.max(1,(lastMessage + maxTimeBetweenData) - Date.now());
      timer = setTimeout(()=>{
        if (lastMessage + maxTimeBetweenData < Date.now())
          reconnectFn();
        else
          scheduleCheck(reconnectFn);
      }, delay);
    };
    return {
      start: scheduleCheck,
      stop: () => clearTimeout(timer),
      reset: () => { lastMessage = Date.now(); }
    };
  },

  createRetryer(initialRetries = 3, initialBackoff = 1000, maxBackoff = 60000) {
    let retriesLeft = initialRetries;
    let backoff = initialBackoff;
    let timer;
    return {
      retry: (fn) => {
        if (retriesLeft < 1)
          return false;
        retriesLeft -= 1;
        timer = setTimeout(fn, backoff);
        backoff = Math.min(maxBackoff, backoff * 2);
        return true;
      },
      reset: () => {
        retriesLeft = initialRetries;
        backoff = initialBackoff;
        clearTimeout(timer);
      }
    };
  },

  getQueryParams(params) {
    return Object.keys(params).filter(key => params[key] != undefined).map(key => encodeURIComponent(key)+"="+encodeURIComponent(params[key])).join("&");
  },

  createUrl(uri, params = {}) {
    const query = module.exports.getQueryParams(params);
    if (query)
      return uri + "?" + query;
    return uri;
  },

  Deferred
};
