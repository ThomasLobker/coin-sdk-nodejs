const jsrsasign = require('jsrsasign');

const utils = require('./utils');

module.exports = class Security {
  constructor(consumerName, privateKey, sharedSecret, validPeriodInSeconds = 30) {
    this.consumerName = consumerName;
    this.hmacSecret = Buffer.from(utils.getFileContent(sharedSecret), 'base64').toString('hex');
    this.privateKey = jsrsasign.KEYUTIL.getKey(utils.getFileContent(privateKey));
    this.validPeriodInSeconds = validPeriodInSeconds;
  }

  generateHmacMessage(headers, username, key, requestLine) {
    const mac = new jsrsasign.KJUR.crypto.Mac({ alg: 'HmacSHA256', pass: key });
    var message = Object.keys(headers).map((key) => `${key}: ${headers[key]}`).join('\n') + '\n' + requestLine;
    mac.updateString(message);
    const hex = mac.doFinal();
    const signature = Buffer.from(hex, 'hex').toString('base64');
    const headersString = Object.keys(headers).join(' ');
    return `hmac username="${username}", algorithm="hmac-sha256", headers="${headersString} request-line", signature="${signature}"`;
  }

  generateHmac(method, path, date, additionalHeaders) {
    const headers = { 'x-date': date };
    const sharedKey = jsrsasign.KJUR.crypto.Cipher.decrypt(this.hmacSecret, this.privateKey, 'RSA');
    return this.generateHmacMessage(Object.assign(headers, additionalHeaders), this.consumerName, sharedKey, `${method} ${path} HTTP/1.1`);
  }

  generateDigest(body) {
    const mac = new jsrsasign.KJUR.crypto.MessageDigest({ alg: 'sha256', prov: 'cryptojs' });
    const hash = mac.digestString(body && JSON.stringify(body));
    const digest = Buffer.from(hash, 'hex').toString('base64');
    return `SHA-256=${digest}`;
  }

  generateToken() {
    const oHeader = { alg: 'RS256', typ: 'JWT' };
    const oPayload = {
      iss: this.consumerName,
      nbf: parseInt((Date.now() / 1000).toString(), 0) - 30,
      exp: parseInt((Date.now() / 1000).toString(), 0) + 30 + this.validPeriodInSeconds
    };
    return jsrsasign.KJUR.jws.JWS.sign('RS256', JSON.stringify(oHeader), JSON.stringify(oPayload), this.privateKey);
  }

  generateHeaders(method, url, body, additionalHeaders = null) {
    const date = (new Date()).toUTCString();
    const path = "/" + url.split('/').slice(3).join('/').split('?')[0];
    const digest = this.generateDigest(body || '');
    const signature = this.generateHmac(method, path, date, { digest });

    const jwtToken = this.generateToken();

    return Object.assign({
      Authorization: signature,
      digest: digest,
      'x-date': date,
      'Cookie': `jwt=${jwtToken}; path=/number-portability/v1/dossiers/portingrequest`
    }, additionalHeaders);
  }
};
