const request = require('request-promise');
const Security = require('./security');

class RestApiClient {
  constructor(consumerName, privateKeyFile, encryptHmacSecretFile, validPeriodInSeconds = 30) {
    this.security = new Security(consumerName, privateKeyFile, encryptHmacSecretFile, validPeriodInSeconds);
  }

  //Release script will update the version number, changing it in the code will break this.
  static getFullVersion() {
    return 'coin-sdk-nodejs-1.0.1';
  }

  sendWithToken(method, url, content, additionalHeaders = {}) {
    return request(url, {
      method,
      json: true,
      body: content,
      headers: this.security.generateHeaders(method, url, content, Object.assign({}, additionalHeaders, {"User-Agent": RestApiClient.getFullVersion()})),
    });
  }
}

module.exports = RestApiClient;
