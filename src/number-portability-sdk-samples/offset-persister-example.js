class OffsetPersisterExample {

  constructor() {
    this.persistedOffset = -1;
  }

  getPersistedOffset() {
    return this.persistedOffset;
  }

  persistOffset(offset) {
    this.persistedOffset = offset;
  }
}

module.exports = OffsetPersisterExample;
