const env = require('../common-sdk/env');
env.init();

const NumberPortabilityService = require('../number-portability-sdk/service/service');
const PortingRequestBuilder = require('../number-portability-sdk/messages/v1/builder/PortingRequestBuilder');
const PortingRequestAnswerBuilder = require('../number-portability-sdk/messages/v1/builder/PortingRequestAnswerBuilder');
const PortingPerformedBuilder = require('../number-portability-sdk/messages/v1/builder/PortingPerformedBuilder');

const MessageResponse = require('../number-portability-sdk/messages/v1/model/MessageResponse');

const consumerName = require('../common-sdk/env').get("CONSUMER_NAME");
const privateKeyFile = require('../common-sdk/env').get("PRIVATE_KEY_FILE");
const encryptedHmacSecretFile = require('../common-sdk/env').get("SHARED_SECRET_FILE");
const validPeriodInSeconds = 30;
const coinBaseUrl = require('../common-sdk/env').get("API_ENDPOINT");

const senderNetworkOperator = 'DEMO1';
const senderServiceProvider = senderNetworkOperator;
const receiverNetworkOperator = 'DEMO2';
const receiverServiceProvider = receiverNetworkOperator;

// Demo dossier-id as an example, in real world scenario's. The id should be generated an unique (in case of a PortingRequest)
// or the dossier-id should match the received dossier-id (in case of a PortingRequestAnswer)
const dossierId = 'DEMO1-123456';

// For Demo purposes the next constants are defined to show the working. In real life secanrio's the number ranges must
// match the number range of the donor/recipient and als the timestamp should be generated before the message is sent.
const startNumberRange = "0612345678";
const endNumberRange = "0612345678";

class MessageSenderExample {

  constructor() {
    this.service = new NumberPortabilityService(consumerName, privateKeyFile, encryptedHmacSecretFile, validPeriodInSeconds, coinBaseUrl);
  }

  sendPortingRequestMessage() {
    const builder = new PortingRequestBuilder();
    const message = builder
      .setFullHeader(senderNetworkOperator, senderServiceProvider, receiverNetworkOperator, receiverServiceProvider)
      .setDossierId(dossierId)
      .setRecipientNetworkOperator(senderNetworkOperator)
      .addPortingRequestSequence()
        .setNumberSeries(startNumberRange, endNumberRange)
        .finish()
      .build();

    this.service.sendMessage(message).then(function(result) {
      const response = {};
      MessageResponse.constructFromObject(result, response);
      console.log(response.transactionId);
    });
  }

  sendPortingRequestAnswerMessage() {
    const builder = new PortingRequestAnswerBuilder();
    const message = builder
      .setFullHeader(receiverNetworkOperator, receiverServiceProvider, senderNetworkOperator, senderServiceProvider)
      .setDossierId(dossierId)
      .setBlocking("N")
      .addPortingRequestAnswerSequence()
        .setNumberSeries(startNumberRange, endNumberRange)
        .finish()
      .build();

    this.service.sendMessage(message).then(function(result) {
      const response = {};
      MessageResponse.constructFromObject(result, response);
      console.log(response.transactionId);
    });
  }

  sendPortingPerformedMessage() {
    const builder = new PortingPerformedBuilder();
    const message = builder
      .setFullHeader(senderNetworkOperator, senderServiceProvider, receiverNetworkOperator, receiverServiceProvider)
      .setDossierId(dossierId)
      .setRecipientNetworkOperator(senderNetworkOperator)
      .setDonorNetworkOperator(receiverNetworkOperator)
      .addPortingPerformedSequence()
        .setNumberSeries(startNumberRange, endNumberRange)
        .finish()
      .build();

    this.service.sendMessage(message).then(function(result) {
      const response = {};
      MessageResponse.constructFromObject(result, response);
      console.log(response.transactionId);
    });
  }

}

const sample1 = new MessageSenderExample();
sample1.sendPortingRequestMessage();
sample1.sendPortingRequestAnswerMessage();
sample1.sendPortingPerformedMessage();
