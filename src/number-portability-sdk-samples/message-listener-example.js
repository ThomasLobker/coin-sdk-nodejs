const ActivationServiceNumberMessage = require('../number-portability-sdk/messages/v1/model/ActivationServiceNumberMessage');
const CancelMessage = require('../number-portability-sdk/messages/v1/model/CancelMessage');
const DeactivationMessage = require('../number-portability-sdk/messages/v1/model/DeactivationMessage');
const DeactivationServiceNumberMessage = require('../number-portability-sdk/messages/v1/model/DeactivationServiceNumberMessage');
const EnumActivationNumberMessage = require('../number-portability-sdk/messages/v1/model/EnumActivationNumberMessage');
const EnumActivationOperatorMessage = require('../number-portability-sdk/messages/v1/model/EnumActivationOperatorMessage');
const EnumActivationRangeMessage = require('../number-portability-sdk/messages/v1/model/EnumActivationRangeMessage');
const EnumDeactivationNumberMessage = require('../number-portability-sdk/messages/v1/model/EnumDeactivationNumberMessage');
const EnumDeactivationOperatorMessage = require('../number-portability-sdk/messages/v1/model/EnumDeactivationOperatorMessage');
const EnumDeactivationRangeMessage = require('../number-portability-sdk/messages/v1/model/EnumDeactivationRangeMessage');
const EnumProfileActivationMessage = require('../number-portability-sdk/messages/v1/model/EnumProfileActivationMessage');
const EnumProfileDeactivationMessage = require('../number-portability-sdk/messages/v1/model/EnumProfileDeactivationMessage');
const ErrorFoundMessage = require('../number-portability-sdk/messages/v1/model/ErrorFoundMessage');
const PortingPerformedMessage = require('../number-portability-sdk/messages/v1/model/PortingPerformedMessage');
const PortingRequestMessage = require('../number-portability-sdk/messages/v1/model/PortingRequestMessage');
const PortingRequestAnswerMessage = require('../number-portability-sdk/messages/v1/model/PortingRequestAnswerMessage');
const PortingRequestAnswerDelayedMessage = require('../number-portability-sdk/messages/v1/model/PortingRequestAnswerDelayedMessage');
const RangeActivationMessage = require('../number-portability-sdk/messages/v1/model/RangeActivationMessage');
const RangeDeactivationMessage = require('../number-portability-sdk/messages/v1/model/RangeDeactivationMessage');
const TariffChangeServiceNumberMessage = require('../number-portability-sdk/messages/v1/model/TariffChangeServiceNumberMessage');

class MessageListenerExample {

  onActivationsn(message) {
    const activationSnMessage = ActivationServiceNumberMessage.constructFromObject(JSON.parse(message.data).message);
    console.log(`${message.lastEventId} - ${JSON.stringify(activationSnMessage)}`);
  }

  onCancel(message) {
    const cancelMessage = CancelMessage.constructFromObject(JSON.parse(message.data).message);
    console.log(`${message.lastEventId} - ${JSON.stringify(cancelMessage)}`);
  }

  onDeactivation(message) {
    const deactivationMessage = DeactivationMessage.constructFromObject(JSON.parse(message.data).message);
    console.log(`${message.lastEventId} - ${JSON.stringify(deactivationMessage)}`);
  }

  onDeactivationsn(message) {
    const deactivationSnMessage = DeactivationServiceNumberMessage.constructFromObject(JSON.parse(message.data).message);
    console.log(`${message.lastEventId} - ${JSON.stringify(deactivationSnMessage)}`);
  }

  onEnumActivationNumber(message) {
    const enumActivationNumberMessage = EnumActivationNumberMessage.constructFromObject(JSON.parse(message.data).message);
    console.log(`${message.lastEventId} - ${JSON.stringify(enumActivationNumberMessage)}`);
  }

  onEnumActivationOperator(message) {
    const enumActivationOperatorMessage = EnumActivationOperatorMessage.constructFromObject(JSON.parse(message.data).message);
    console.log(`${message.lastEventId} - ${JSON.stringify(enumActivationOperatorMessage)}`);
  }

  onEnumActivationRange(message) {
    const enumActivationRangeMessage = EnumActivationRangeMessage.constructFromObject(JSON.parse(message.data).message);
    console.log(`${message.lastEventId} - ${JSON.stringify(enumActivationRangeMessage)}`);
  }

  onEnumDeactivationNumber(message) {
    const enumDeactivationNumberMessage = EnumDeactivationNumberMessage.constructFromObject(JSON.parse(message.data).message);
    console.log(`${message.lastEventId} - ${JSON.stringify(enumDeactivationNumberMessage)}`);
  }

  onEnumDeactivationOperator(message) {
    const enumDeactivationOperatorMessage = EnumDeactivationOperatorMessage.constructFromObject(JSON.parse(message.data).message);
    console.log(`${message.lastEventId} - ${JSON.stringify(enumDeactivationOperatorMessage)}`);
  }

  onEnumDeactivationRange(message) {
    const enumDeactivationRangeMessage = EnumDeactivationRangeMessage.constructFromObject(JSON.parse(message.data).message);
    console.log(`${message.lastEventId} - ${JSON.stringify(enumDeactivationRangeMessage)}`);
  }

  onEnumProfileActivation(message) {
    const enumProfileActivation = EnumProfileActivationMessage.constructFromObject(JSON.parse(message.data).message);
    console.log(`${message.lastEventId} - ${JSON.stringify(enumProfileActivation)}`);
  }

  onEnumProfileDeactivation(message) {
    const enumProfileDeactivation = EnumProfileDeactivationMessage.constructFromObject(JSON.parse(message.data).message);
    console.log(`${message.lastEventId} - ${JSON.stringify(enumProfileDeactivation)}`);
  }

  onErrorFound(message) {
    const errorFound = ErrorFoundMessage.constructFromObject(JSON.parse(message.data).message);
    console.log(`${message.lastEventId} - ${JSON.stringify(errorFound)}`);
  }

  onPortingPerformed(message) {
    const portingPerformed = PortingPerformedMessage.constructFromObject(JSON.parse(message.data).message);
    console.log(`${message.lastEventId} - ${JSON.stringify(portingPerformed)}`);
  }

  onPortingRequest(message) {
    const portingRequest = PortingRequestMessage.constructFromObject(JSON.parse(message.data).message);
    console.log(`${message.lastEventId} - ${JSON.stringify(portingRequest)}`);
  }

  onPortingRequestAnswer(message) {
    const portingRequestAnswer = PortingRequestAnswerMessage.constructFromObject(JSON.parse(message.data).message);
    console.log(`${message.lastEventId} - ${JSON.stringify(portingRequestAnswer)}`);
  }

  onPortingRequestAnswerDelayed(message) {
    const portingRequestAnswerDelayed = PortingRequestAnswerDelayedMessage.constructFromObject(JSON.parse(message.data).message);
    console.log(`${message.lastEventId} - ${JSON.stringify(portingRequestAnswerDelayed)}`);
  }

  onRangeActivation(message) {
    const rangeActivationMessage = RangeActivationMessage.constructFromObject(JSON.parse(message.data).message);
    console.log(`${message.lastEventId} - ${JSON.stringify(rangeActivationMessage)}`);
  }

  onRangeDeactivation(message) {
    const rangeDeactivationMessage = RangeDeactivationMessage.constructFromObject(JSON.parse(message.data).message);
    console.log(`${message.lastEventId} - ${JSON.stringify(rangeDeactivationMessage)}`);
  }

  onTariffChangeSn(message) {
    const tariffChangeServiceNumberMessage = TariffChangeServiceNumberMessage.constructFromObject(JSON.parse(message.data).message);
    console.log(`${message.lastEventId} - ${JSON.stringify(tariffChangeServiceNumberMessage)}`);
  }

  onProcess(message) {
    console.log(`${message.lastEventId} - ${JSON.stringify(message)}`);
  }
}

module.exports = MessageListenerExample;
