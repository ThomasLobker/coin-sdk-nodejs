const NumberPortabilityMessageConsumer = require('../number-portability-sdk/service/consumer');
const OffsetPersister = require('./offset-persister-example');
const MessageListenerExample = require('./message-listener-example');

// const coinBaseUrl = "https://test-api.coin.nl";
const coinBaseUrl = "http://localhost:8000";
// const consumerName = "API_T_DEMO2";
const consumerName = "loadtest-loada";
// const privateKeyFile = "../../keys/demo2-private-key.pem";
const privateKeyFile = "../../keys/private-key.pem";
// const encryptedHmacSecretFile = "../../keys/demo2-sharedkey.encrypted";
const encryptedHmacSecretFile = "../../keys/sharedkey.encrypted";
const validPeriodInSeconds = 30;


class MessageConsumerExample {
  constructor() {
    this.consumer = new NumberPortabilityMessageConsumer(consumerName, privateKeyFile, encryptedHmacSecretFile, coinBaseUrl, 30, 10, validPeriodInSeconds);
  }

  run() {
    return this.consumer.startConsuming(new MessageListenerExample(), {offsetPersister: new OffsetPersister()});
  }
}

const sample2 = new MessageConsumerExample();
const disposable = sample2.run();

disposable.promise.then(()=>{console.log("stream closed");});

setTimeout(()=>disposable.close(), 5000);
