#!/bin/bash

if [[ $RUN_LOCALLY != true ]] ; then
	/app/src/bin/copy-ssm-parameters --recursive --path /dev-api/kong/consumers/crdb-rest/loadtest --directory /app/src/keys > /dev/null
fi

exec node /app/src/index.js $@
